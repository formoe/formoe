// @flow
export const waitForMillis = async (millis: number) => {
  await new Promise((resolve) => setTimeout(resolve, millis))
}
