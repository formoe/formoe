import {
  toBeInTheDocument,
  toHaveClass,
  toHaveTextContent,
} from "@testing-library/jest-dom/matchers"

expect.extend({ toBeInTheDocument, toHaveClass, toHaveTextContent })
