# React Comons library

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)

This project hosts components that can be shared between react projects.

## Development

This is a lerna mono repo. Components from these projects are deployed to the public npm registry and can be pulled from there.

TL;DR: [Check the cheat sheet](CheatSheet.md) and [common pitfalls](Pitfalls.md)

### Commits

Commits have to adhere to [conventional commits specification](https://www.conventionalcommits.org/en/v1.0.0/#summary) based on [@commitlint/config-conventional](https://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-conventional#rules) and [@commitlint/config-lerna-scopes](https://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-lerna-scopes#commitlintconfig-lerna-scopes) to ensure correct versioning.

For ease of use [commitizen](https://github.com/commitizen/cz-cli) helps adhering to the correct format. Use `npm run commit` for commits.

If you need to retry a commit for any reason (failing commit hooks for example), you can use `npm run recommit`.

Finally [commit-lint](https://commitlint.js.org/) will check every commit message before accepting commit. That way you can use your favourite git interface, but you have to adhere to the format yourself.

### Creating new packages

For creating new packages convinient npm scripts are provided.

[Check the cheat sheet](CheatSheet.md#setup-new-package) to see which script best suits your case and on how to develop your new package.

Running one of the scripts, a new folder will be created under the `packages` folder. The contents of the corresponding `package-template` are copied there, and the relevant placeholders are replaced with the given parameters.

The template packages `package.json` are configured with a specific set of files to publish like:

```JSON
"files": [
    "dist"
  ]
```

Thus only the preconfigured files are considered for packaging. If you need to include more files add them to the `files` array.

### Installing dependencies

#### single dependencies

For specific dependencies only needed by a single package just use `npm run add -- [package] --scope=[consumer]` where `[consumer]` is replaced with the package you want to install into.

Use `npm run add:dev -- [package] --scope=[consumer]` to install a dev dependency.

#### one package to rule them all

To add new dependencies for all packages use `npm run add -- [package]`.
The `bootstrap` command ist configured in [lerna.json](lerna.json) to hoist automatically.

Remember, that all `prod` and `cli` dependencies need to be specified in a components `package.json`, even if they are accessible via hoisting!
For `cli` dependencies consider using `npx` instead of installing them locally. This may not always be the best option though.

### Installing all dependencies locally

To install all the dependencies from the repo use `npm run bootstrap` instead of `npm install`.

### Lerna

The project was initialised with `npx lerna init --independent` to setup lerna structure
with independant sub packages.

We use this, as all components have utility character and are completely independant from each other

#### Publish

`npm run publish` is used to publish the lib as npm packages during CI.

As we use the scope `@formoe`, all packages are published with public access to the [Formoe organisation at npmjs.com](https://www.npmjs.com/org/formoe) automatically.

We use the following configuration in [lerna.json](lerna.json):

- `ignoreChanges` for various utility files like tests, readmes or stories, that should not trigger new releases when changed
- `message` to use a commit message for lerna commits adhering to [conventional commits specification](https://www.conventionalcommits.org/en/v1.0.0/)
- `conventionalCommits` to retreive version numbers using `semantic versioning` from the commit history.

We use the following flags in [package.json](package.json):

- `--no-commit-hooks` to prevent triggering commit hooks for lerna commits
- `--yes` prevents the cli from prompting anything to automatically release from CI

We use the following parameters in `publish.yml`:

- `--create-release gitlab` to create an official gitlab release

### Jest

Jest is used to run all tests in the repo with `npm run test`.

### Babel

Babel is used as an ES2015 compiler.

Babel is configured at project root using the [babel.config.json](babel.config.json).

#### Presets

Babel is configured project wide with the following presets:

- @babel/preset-env
  - `bugfixes` is set to true to only compile down to the closests supported modern syntax by targets
- @babel/preset-flow to strip `flow` typings
- @babel/preset-react

#### Sub package overrides

`babelrcRoots` is configured so that `.babelrc.json` files can be used on a per package basis to override package specific settings for babel.

### Linting

`npm run lint` run styles as well as js linter.

#### ESLint

ESLint is used to lint js files. Configuration can be found at [.eslintrc.json](.eslintrc.json).
Run it with `npm run lint:js`

#### stylelint

Stylelint is used to ensure scss code quality. Configuration can be found at [.stylelintrc.json](.stylelintrc.json).
Run it with `npm run lint:styles`

[stylelint-config-sass-guidelines](https://github.com/bjankord/stylelint-config-sass-guidelines) is used to extend lint rules.

### Flow

[Flow](https://flow.org/) is used as static type checker.

Run a complete check with `npm run flow`.

Jest libdefs were installed using `npx flow-typed install jest@26.x.x` to support test files.

### Storybook

[Storybook](https://storybook.js.org/) is used to preview UI components.

Start the server with `npm run storybook`. You can now view your stories on <http://localhost:6006>

New stories need to be placed in a `stories` sub directory to be picked up. This ensures they are excluded from various infrastructure tasks of the repo.

For writing stories we use the [Component Story Format](https://storybook.js.org/docs/formats/component-story-format/).

### Commit hooks

[husky](https://github.com/typicode/husky#readme) and [lint-staged](https://github.com/okonet/lint-staged#readme) are used to automatically trigger tasks when committing. Configured tasks are:

- linting
- flow type checks
- test
- prettier formatting

### CD

#### Gitlab CI

`NPM_AUTH_TOKEN` is set to a publish token retreived from the npm org user.

`NPM_READ_ONLY_AUTH_TOKEN` is set to a read only token retreived from the npm org user. This token also needs to be set in projects consuming the lib to access the npm packages.

##### write access to the repo for publishing scripts

As `ssh` write access doesn't seem to work so far in gitlab ci runner for some reason, authentication to update the version fields in packages and create gitlab releases via `lerna publish` is achieved by using a personal access token of the ci user `jav3000`.

The token is set in the variable `GL_TOKEN` and used in the [publish.yml](infrastructure/publish.yml).

### Editor integration

#### VSCode

- Added jsconfig.json to support VS Code IDE.
- Added support vor test watching via `.vscode/launch.json`

The following plugins can be installed to support development:

- [Flow Language Support](https://marketplace.visualstudio.com/items?itemName=flowtype.flow-for-vscode) => In addition open the settings, search for `javascript.validate.enable` and disable it to get rid of false typescript errors.
- [vscode-stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint)

  Add the following configuration to your `settings.json`

  ```JSON
  "css.validate": false,
  "scss.validate": false,
  "less.validate": false
  ```
