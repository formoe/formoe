# use-app-messages

A hook providing a context wrapper and api to set and manage messages.

A `Message` is an object containing a `text` and a `type`.

Create the context like this:

```JSX
import { MessageProvider } from "./index"

<MessageProvider>
  <YourComponent />
</MessageProvider>
```

now in the component tree inside the provider you can:

```JS
import { useAppMessages, MESSAGE_TYPES } from "./index"

const { addMessage, acknowledge, messages } = useAppMessages()
```

and then:

- add a `Message` via e.g `addMessage({text: "some text", type: MESSAGE_TYPES.info})`
- acknowledge a `Message` via e.g. `acknowledge(0)`
- read all `messages`

## AppMessages

With:

```JSX
import { AppMessages } from "./index"

<AppMessages />
```

you can integrate a [themed component](../theme-default/README.md) using the hook to show all app messages.
