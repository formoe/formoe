// @flow
import React from "react"
import { render } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import { MessageProvider, useAppMessages, MESSAGE_TYPES } from "./index"

function DummyComponent() {
  const { addMessage, acknowledge, messages } = useAppMessages()
  return (
    <div>
      <button
        onClick={() =>
          addMessage({
            text: "Info",
            type: MESSAGE_TYPES.info,
          })
        }
      >
        Add Info
      </button>
      <button
        onClick={() =>
          addMessage({
            text: "Warning",
            type: MESSAGE_TYPES.warning,
          })
        }
      >
        Add Warning
      </button>
      <button
        onClick={() =>
          addMessage({
            text: "Error",
            type: MESSAGE_TYPES.error,
          })
        }
      >
        Add Error
      </button>
      {messages.map((message, idx) => {
        return (
          <div
            key={idx}
            onClick={() => acknowledge(idx)}
            aria-label={`App message #${idx}`}
          >{`${message.text} with type: ${message.type}`}</div>
        )
      })}
    </div>
  )
}

describe("use-app-error-handling", () => {
  it("should let you add and acknowledge messages and render them", () => {
    const {
      getByLabelText,
      queryAllByLabelText,
      queryByLabelText,
      getByRole,
    } = render(
      <MessageProvider>
        <DummyComponent />
      </MessageProvider>
    )
    expect(queryAllByLabelText("App message #")).toStrictEqual([])
    userEvent.click(getByRole("button", { name: "Add Info" }))
    expect(getByLabelText("App message #0")).toHaveTextContent(
      "Info with type: info"
    )
    userEvent.click(getByRole("button", { name: "Add Warning" }))
    expect(getByLabelText("App message #1")).toHaveTextContent(
      "Warning with type: warning"
    )
    userEvent.click(getByRole("button", { name: "Add Error" }))
    expect(getByLabelText("App message #2")).toHaveTextContent(
      "Error with type: error"
    )

    userEvent.click(getByLabelText("App message #0"))

    expect(getByLabelText("App message #0")).toHaveTextContent("Warning")
    expect(getByLabelText("App message #1")).toHaveTextContent("Error")
    expect(queryByLabelText("App message #2")).not.toBeInTheDocument()

    userEvent.click(getByLabelText("App message #1"))
    expect(getByLabelText("App message #0")).toHaveTextContent("Warning")
    expect(queryByLabelText("App message #1")).not.toBeInTheDocument()
    expect(queryByLabelText("App message #2")).not.toBeInTheDocument()
  })
})
