// @flow
import * as React from "react"
import type { Node } from "react"
import { createContext, useState, useContext, useCallback } from "react"

import type { Interface, Props, Message } from "./index.js.flow"

export const MESSAGE_TYPES = {
  error: "error",
  warning: "warning",
  info: "info",
}

const MessageContext = createContext<Interface>({})

export const MessageProvider = ({ children }: Props): Node => {
  const [messages, setMessages] = useState<Message[]>([])

  const addMessage = useCallback((message) => {
    setMessages((messages) => [...messages, message])
  }, [])

  const acknowledge = useCallback((acknowledgedIndex) => {
    setMessages((messages) =>
      messages.filter((_, index) => index !== acknowledgedIndex)
    )
  }, [])

  return (
    <MessageContext.Provider value={{ messages, addMessage, acknowledge }}>
      {children}
    </MessageContext.Provider>
  )
}

export const useAppMessages = (): Interface => useContext(MessageContext)
