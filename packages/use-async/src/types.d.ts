declare module "@formoe/use-async" {

  type AsyncFunction<RequestType, ResponseType> = (request: RequestType) => Promise<ResponseType>

  interface Params<RequestType, ResponseType> {
    initialRequest?: RequestType
    asyncFunction: AsyncFunction<RequestType, ResponseType>
  }

  export interface Result<RequestType, ResponseType, ErrorType> {
    request?: RequestType
    response?: ResponseType
    error?: ErrorType
    inProgress: boolean
  }

  interface Return<RequestType, ResponseType, ErrorType> {
    result: Result<RequestType, ResponseType, ErrorType>
    setRequest: (request?: RequestType) => void
    reset: () => void
  }
  
  function useAsync<RequestType, ResponseType, ErrorType>(params: Params<RequestType, ResponseType>): Return<RequestType, ResponseType, ErrorType>

  export default useAsync

  interface StateSkeletonsProps<ErrorType, ResponseType> {
    children: JSX.Element
    response?: ResponseType
    skeleton?: JSX.Element
    errorConfig?: { error?: ErrorType, skeleton: JSX.Element }
    progressConfig?: { inProgress: boolean, skeleton: JSX.Element }
  }

  export function StateSkeletons<ErrorType, ResponseType>(
    props: StateSkeletonsProps<ErrorType, ResponseType>
  ): JSX.Element

  export function useError<ErrorType>(): ErrorType
  export function useInProgress(): boolean
  export function useResponse<ResponseType>(): ResponseType

  export interface Props<RequestType, ResponseType, ErrorType> {
    children: JSX.Element
    asyncFunction: AsyncFunction<RequestType, ResponseType>
    request?: RequestType
    skeleton?: JSX.Element
    progressSkeleton?: JSX.Element
    errorSkeleton?: JSX.Element
    onSuccess?: (result?: Result<RequestType, ResponseType, ErrorType>) => void
    onError?: (result?: Result<RequestType, ResponseType, ErrorType>) => void
    onProgressChange?: (inProgress?: boolean) => void
  }

  export function AsyncComponentWrapper<
    RequestType,
    ResponseType,
    ErrorType
  >(
    props: Props<RequestType, ResponseType, ErrorType>
  ): JSX.Element

}
