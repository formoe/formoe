// @flow
import React, { createContext, useContext } from "react"
import type { Node } from "react"
import type { StateSkeletonsProps } from "./index.js.flow"

const ErrorContext = createContext<any>()
const ResponseContext = createContext<any>()
const InProgressContext = createContext()

export default function StateSkeletons<ErrorType, ResponseType>({
  children,
  response,
  skeleton,
  errorConfig,
  progressConfig,
}: StateSkeletonsProps<ErrorType, ResponseType>): Node {
  if (errorConfig && errorConfig.error) {
    return (
      <ErrorContext.Provider value={errorConfig.error}>
        {errorConfig.skeleton}
      </ErrorContext.Provider>
    )
  }

  if (!response && skeleton) {
    return skeleton
  }

  if (progressConfig && progressConfig.inProgress) {
    return (
      <InProgressContext.Provider value={progressConfig.inProgress}>
        {progressConfig.skeleton}
      </InProgressContext.Provider>
    )
  }

  return (
    <ResponseContext.Provider value={response}>
      {children}
    </ResponseContext.Provider>
  )
}

export const useError = (): any => useContext(ErrorContext)
export const useInProgress = (): ?boolean => useContext(InProgressContext)
export const useResponse = (): any => useContext(ResponseContext)
