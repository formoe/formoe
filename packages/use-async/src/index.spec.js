// @flow
import React, { useState } from "react"
import { render, waitFor } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import { renderHook, act } from "@testing-library/react-hooks"

import { waitForMillis } from "../../../test/test-utils"

import useAsync, {
  AsyncComponentWrapper,
  StateSkeletons,
  useResponse,
  useError,
  useInProgress,
} from "./index"
import type { AsyncFunction, Result } from "./index"

type ResolveWithValue<ResultType> = {
  response: ResultType,
  respondInMillis?: number,
}

function asyncResponse<RequestType, ResultType>({
  respondInMillis,
  resolveWith,
  resolveWithMap,
  rejectWith,
}: {
  respondInMillis?: number,
  resolveWith?: ResultType,
  resolveWithMap?: Map<RequestType, ResolveWithValue<ResultType>>,
  rejectWith?: any,
}): AsyncFunction<RequestType, ResultType> {
  return jest.fn(async (request?: RequestType) => {
    if (respondInMillis) {
      // respondInMillis can be a simple number or a map of reqwuest to waiting time
      await waitForMillis(respondInMillis)
    }

    if (rejectWith) {
      return Promise.reject(rejectWith)
    }

    let result = resolveWith
    // do we have a request to response mapping?
    if (resolveWithMap && request) {
      const requestMapping = resolveWithMap.get(request)
      if (requestMapping) {
        result = requestMapping.response
        if (requestMapping.respondInMillis) {
          await waitForMillis(requestMapping.respondInMillis)
        }
      }
    }

    return (
      result ||
      Promise.reject(
        "Expected response not found, please define 'resolveWith' or rejectWith 'properties."
      )
    )
  })
}

describe("use-async", () => {
  it("should not start automatically if initial request is omitted", async () => {
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 10,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useAsync({
        asyncFunction,
      })
    )

    const { result: initialResult, setRequest } = hookInterface.current
    expect(initialResult).toStrictEqual({
      request: undefined,
      inProgress: false,
    })
    expect(asyncFunction).not.toHaveBeenCalledWith()

    act(() => {
      setRequest("1337 req")
    })

    const { result: startedResult } = hookInterface.current
    expect(startedResult).toStrictEqual({
      request: "1337 req",
      inProgress: true,
    })
    expect(asyncFunction).toHaveBeenCalledWith("1337 req")
    expect(asyncFunction).toHaveBeenCalledTimes(1)

    await waitForNextUpdate()

    const { result: finishedInitialResult } = hookInterface.current
    expect(finishedInitialResult).toEqual({
      request: "1337 req",
      response: "1337",
      inProgress: false,
    })
  })

  it("should run the initial request", async () => {
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 10,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useAsync<string, string, string>({
        asyncFunction,
        initialRequest: "1337 req",
      })
    )

    const { result: initialResult } = hookInterface.current
    expect(initialResult).toStrictEqual({
      request: "1337 req",
      inProgress: true,
    })
    expect(asyncFunction).toHaveBeenCalledWith("1337 req")
    expect(asyncFunction).toHaveBeenCalledTimes(1)

    await waitForNextUpdate()

    const { result: finishedInitialResult } = hookInterface.current
    expect(finishedInitialResult).toEqual({
      request: "1337 req",
      response: "1337",
      inProgress: false,
    })
  })

  it("should be able to trigger the same request twice in a row", async () => {
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 10,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useAsync<string, string, string>({
        asyncFunction,
        initialRequest: "1337 req",
      })
    )

    const { result: initialResult } = hookInterface.current
    expect(initialResult).toStrictEqual({
      request: "1337 req",
      inProgress: true,
    })
    expect(asyncFunction).toHaveBeenCalledWith("1337 req")
    expect(asyncFunction).toHaveBeenCalledTimes(1)
    await waitForNextUpdate()
    const { result: finishedInitialResult, setRequest } = hookInterface.current
    expect(finishedInitialResult).toEqual({
      request: "1337 req",
      response: "1337",
      inProgress: false,
    })

    // $FlowFixMe: don't bother typing mock functions
    asyncFunction.mockClear()

    act(() => {
      setRequest("1337 req")
    })

    const { result: inProgressResult } = hookInterface.current
    expect(inProgressResult).toEqual({
      request: "1337 req",
      inProgress: true,
    })
    await waitForNextUpdate()
    const { result: secondResult } = hookInterface.current
    expect(secondResult).toEqual({
      request: "1337 req",
      response: "1337",
      inProgress: false,
    })
    expect(asyncFunction).toHaveBeenCalledWith("1337 req")
    expect(asyncFunction).toHaveBeenCalledTimes(1)
  })

  it("should clear the result and cancel running requests on reset", async () => {
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 10,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useAsync<string, string, string>({
        asyncFunction,
        initialRequest: "1337 req",
      })
    )

    const { result: initialResult } = hookInterface.current
    expect(initialResult).toStrictEqual({
      request: "1337 req",
      inProgress: true,
    })
    expect(asyncFunction).toHaveBeenCalledWith("1337 req")
    expect(asyncFunction).toHaveBeenCalledTimes(1)
    await waitForNextUpdate()
    const { result: finishedInitialResult, setRequest } = hookInterface.current
    expect(finishedInitialResult).toEqual({
      request: "1337 req",
      response: "1337",
      inProgress: false,
    })

    // $FlowFixMe: don't bother typing mock functions
    asyncFunction.mockClear()

    act(() => {
      setRequest("1337 req")
    })

    const { result: inProgressResult, reset } = hookInterface.current
    expect(inProgressResult).toEqual({
      request: "1337 req",
      inProgress: true,
    })
    act(() => {
      reset()
    })
    const { result: clearedResult } = hookInterface.current
    expect(clearedResult).toEqual({
      request: undefined,
      inProgress: false,
    })
    expect(asyncFunction).toHaveBeenCalledWith("1337 req")
    expect(asyncFunction).toHaveBeenCalledTimes(1)
  })

  it("should stay inProgress as long as any request runs and only return with the last result", async () => {
    const asyncFunction = asyncResponse({
      respondInMillis: 50,
      resolveWithMap: new Map([
        ["request 0", { response: "response 0" }],
        ["request 1", { response: "response 1" }],
        ["request 2", { response: "response 2" }],
      ]),
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useAsync<string, string, string>({
        asyncFunction,
        initialRequest: "request 0",
      })
    )

    const { result: initialResult, setRequest } = hookInterface.current
    expect(initialResult).toStrictEqual({
      request: "request 0",
      inProgress: true,
    })

    await waitForMillis(10)

    act(() => {
      setRequest("request 1")
    })

    await waitForMillis(10)

    act(() => {
      setRequest("request 2")
    })

    await waitForNextUpdate()

    const { result: finishedInitialResult } = hookInterface.current
    expect(finishedInitialResult).toEqual({
      request: "request 2",
      response: "response 2",
      inProgress: false,
    })
    expect(asyncFunction).toHaveBeenCalledWith("request 0")
    expect(asyncFunction).toHaveBeenCalledWith("request 1")
    expect(asyncFunction).toHaveBeenCalledWith("request 2")
    expect(asyncFunction).toHaveBeenCalledTimes(3)
  })

  it("should only consider the last request even if older requests return later", async () => {
    // eslint-disable-next-line no-undef
    const errorSpy = jest.spyOn(global.console, "error")
    const asyncFunction = asyncResponse({
      resolveWithMap: new Map([
        [
          "request 0",
          {
            response: "response 0",
            respondInMillis: 50,
          },
        ],
        [
          "request 1",
          {
            response: "response 1",
            respondInMillis: 10,
          },
        ],
      ]),
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useAsync<string, string, string>({
        asyncFunction,
        initialRequest: "request 0",
      })
    )

    const { result: initialResult, setRequest } = hookInterface.current
    expect(initialResult).toStrictEqual({
      request: "request 0",
      inProgress: true,
    })

    await waitForMillis(10)

    act(() => {
      setRequest("request 1")
    })

    await waitForNextUpdate()

    const { result: finishedInitialResult } = hookInterface.current
    expect(finishedInitialResult).toEqual({
      request: "request 1",
      response: "response 1",
      inProgress: false,
    })
    expect(asyncFunction).toHaveBeenCalledWith("request 0")
    expect(asyncFunction).toHaveBeenCalledWith("request 1")
    expect(asyncFunction).toHaveBeenCalledTimes(2)
    // we would get console errors if act would occur outside of test
    expect(errorSpy).not.toHaveBeenCalled()
  })

  it("should only consider the last request even if requests overlap and not change state in between", async () => {
    // eslint-disable-next-line no-undef
    const errorSpy = jest.spyOn(global.console, "error")
    const asyncFunction = asyncResponse({
      resolveWithMap: new Map([
        [
          "request 0",
          {
            response: "response 0",
            respondInMillis: 20,
          },
        ],
        [
          "request 1",
          {
            response: "response 1",
            respondInMillis: 20,
          },
        ],
      ]),
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useAsync<string, string, string>({
        asyncFunction,
        initialRequest: "request 0",
      })
    )

    const { result: initialResult, setRequest } = hookInterface.current
    expect(initialResult).toStrictEqual({
      request: "request 0",
      inProgress: true,
    })

    await waitForMillis(10)

    act(() => {
      setRequest("request 1")
    })

    await waitForNextUpdate()

    const { result: finishedInitialResult } = hookInterface.current
    expect(finishedInitialResult).toEqual({
      request: "request 1",
      response: "response 1",
      inProgress: false,
    })
    expect(asyncFunction).toHaveBeenCalledWith("request 0")
    expect(asyncFunction).toHaveBeenCalledWith("request 1")
    expect(asyncFunction).toHaveBeenCalledTimes(2)
    // we would get console errors if act would occur outside of test
    expect(errorSpy).not.toHaveBeenCalled()
  })

  describe("AsyncComponentWrapper", () => {
    describe("if no skeletons are provided", () => {
      it("should render the children immediately and then call the success callback", async () => {
        const success = jest.fn()

        const { getByText } = render(
          <AsyncComponentWrapper
            asyncFunction={asyncResponse({ resolveWith: true })}
            onSuccess={success}
            request={null}
          >
            <div>success</div>
          </AsyncComponentWrapper>
        )
        expect(getByText("success")).toBeInTheDocument()
        await waitFor(() =>
          expect(success).toHaveBeenCalledWith({
            request: null,
            inProgress: false,
            response: true,
          })
        )
      })

      it("should render the children on error and call the error callback", async () => {
        const error = jest.fn()

        const { findByText } = render(
          <AsyncComponentWrapper
            asyncFunction={asyncResponse({ rejectWith: "fail" })}
            onError={error}
            request={null}
          >
            <div>success</div>
          </AsyncComponentWrapper>
        )
        expect(await findByText("success")).toBeInTheDocument()
        await waitFor(() =>
          expect(error).toHaveBeenCalledWith({
            error: "fail",
            inProgress: false,
            request: null,
          })
        )
      })
    })

    function DummyWrapper<ResultType>({
      asyncFunction,
      progressChange,
      success,
    }: {
      asyncFunction: AsyncFunction<string, ResultType>,
      progressChange: (inProgress?: boolean) => void,
      success?: (result?: Result<string, ResultType, string>) => void,
    }) {
      const [request, setRequest] = useState(0)
      const progressSkeleton = (
        <>
          <div>current request: {request}</div>
          <button
            onClick={() => {
              setRequest((currentRequest) => currentRequest + 1)
            }}
          >
            start next request while running
          </button>
        </>
      )

      return (
        <AsyncComponentWrapper
          asyncFunction={asyncFunction}
          request={`request ${request}`}
          progressSkeleton={progressSkeleton}
          onProgressChange={progressChange}
          onSuccess={success}
        >
          <button
            onClick={() => {
              setRequest((currentRequest) => currentRequest + 1)
            }}
          >
            start next request after success
          </button>
        </AsyncComponentWrapper>
      )
    }

    describe("if an inProgress skeleton is provided", () => {
      it("should render the progress skeleton when working and children on success and call the progress change callback", async () => {
        const asyncFunction = asyncResponse({
          respondInMillis: 10,
          resolveWith: true,
        })
        const progressChange = jest.fn()

        const { findByText } = render(
          <DummyWrapper
            asyncFunction={asyncFunction}
            progressChange={progressChange}
          />
        )
        expect(
          await findByText("start next request while running")
        ).toBeInTheDocument()
        const buttonElement = await findByText(
          "start next request after success"
        )
        expect(asyncFunction).toHaveBeenCalledWith("request 0")
        expect(progressChange).toHaveBeenCalledWith(true)
        expect(progressChange).toHaveBeenCalledWith(false)
        expect(progressChange).toHaveBeenCalledTimes(2)

        progressChange.mockClear()

        userEvent.click(buttonElement)

        expect(
          await findByText("start next request while running")
        ).toBeInTheDocument()
        expect(
          await findByText("start next request after success")
        ).toBeInTheDocument()
        expect(asyncFunction).toHaveBeenCalledWith("request 1")
        expect(progressChange).toHaveBeenCalledWith(true)
        expect(progressChange).toHaveBeenCalledWith(false)
        expect(progressChange).toHaveBeenCalledTimes(2)
      })

      it("should render the children on error", async () => {
        const { findByText } = render(
          <AsyncComponentWrapper
            asyncFunction={asyncResponse({ rejectWith: "fail" })}
          >
            <div>success</div>
          </AsyncComponentWrapper>
        )
        expect(await findByText("success")).toBeInTheDocument()
      })
    })

    describe("if a skeleton is provided", () => {
      it("should render the skeleton until initialized successfully and then call the callback", async () => {
        const skeleton = <div>initializing</div>
        const progressSkeleton = <div>working hard</div>

        const success = jest.fn()

        const { findByText } = render(
          <AsyncComponentWrapper
            asyncFunction={asyncResponse({
              respondInMillis: 100,
              resolveWith: true,
            })}
            skeleton={skeleton}
            progressSkeleton={progressSkeleton}
            onSuccess={success}
            request={null}
          >
            <div>success</div>
          </AsyncComponentWrapper>
        )
        expect(await findByText("initializing")).toBeInTheDocument()
        expect(await findByText("success")).toBeInTheDocument()
        expect(success).toHaveBeenCalledWith({
          request: null,
          inProgress: false,
          response: true,
        })
      })

      it("should render the skeleton on error and call the callback", async () => {
        const skeleton = <div>initializing</div>
        const progressSkeleton = <div>working hard</div>
        const error = jest.fn()

        const { getByText } = render(
          <AsyncComponentWrapper
            asyncFunction={asyncResponse({ rejectWith: "fail" })}
            skeleton={skeleton}
            progressSkeleton={progressSkeleton}
            onError={error}
            request={null}
          >
            <div>success</div>
          </AsyncComponentWrapper>
        )
        expect(getByText("initializing")).toBeInTheDocument()
        await waitFor(() =>
          expect(error).toHaveBeenCalledWith({
            request: null,
            inProgress: false,
            error: "fail",
          })
        )
      })
    })

    describe("if an error skeleton is provided", () => {
      it("should render the error skeleton on async errors and call the callback", async () => {
        const skeleton = <div>initializing</div>
        const progressSkeleton = <div>working hard</div>
        const errorSkeleton = <div>error</div>

        const error = jest.fn()

        const { findByText } = render(
          <AsyncComponentWrapper
            asyncFunction={asyncResponse({
              rejectWith: "fail",
              respondInMillis: 100,
            })}
            skeleton={skeleton}
            errorSkeleton={errorSkeleton}
            progressSkeleton={progressSkeleton}
            onError={error}
            request={null}
          >
            <div>success</div>
          </AsyncComponentWrapper>
        )
        expect(await findByText("initializing")).toBeInTheDocument()
        expect(await findByText("error")).toBeInTheDocument()
        expect(error).toHaveBeenCalledWith({
          error: "fail",
          inProgress: false,
          request: null,
        })
      })
    })

    it("should hand the request to the async function", async () => {
      const asyncFunction = asyncResponse({ resolveWith: true })

      const { findByText } = render(
        <AsyncComponentWrapper asyncFunction={asyncFunction} request="Foo">
          <div>success</div>
        </AsyncComponentWrapper>
      )
      expect(await findByText("success")).toBeInTheDocument()
      expect(asyncFunction).toHaveBeenCalledWith("Foo")
    })

    // this would throw errors if async cancellation wouldn't work
    it("should cancel async", async () => {
      // eslint-disable-next-line no-undef
      const errorSpy = jest.spyOn(global.console, "error")
      const skeleton = <div>initializing</div>
      const { getByText, unmount } = render(
        <AsyncComponentWrapper
          asyncFunction={asyncResponse({
            respondInMillis: 10,
            resolveWith: true,
          })}
          skeleton={skeleton}
        >
          <div>success</div>
        </AsyncComponentWrapper>
      )
      expect(getByText("initializing")).toBeInTheDocument()
      unmount()
      await waitForMillis(100)
      expect(errorSpy).not.toHaveBeenCalled()
    })
  })

  describe("StateSkeletons", () => {
    it("should render simple skeletons", () => {
      const { getByText } = render(
        <StateSkeletons response={"success"}>
          <div>success</div>
        </StateSkeletons>
      )

      expect(getByText("success")).toBeInTheDocument()
    })

    const ResponseSkeleton = () => {
      const response = useResponse()
      return <div>{response}</div>
    }

    const ErrorSkeleton = () => {
      const error = useError()
      return <div>{error ? error : "not rendered"}</div>
    }

    const ProgressSkeleton = () => {
      const inProgress = useInProgress()
      return <div>{inProgress ? "yes" : "not rendered"}</div>
    }

    it("should provide response via context hooks and work nested", () => {
      const { getByText, queryByText } = render(
        <StateSkeletons response={"success"}>
          <ResponseSkeleton />
          <StateSkeletons
            response={"foo"}
            errorConfig={{ skeleton: <ErrorSkeleton /> }}
            progressConfig={{
              inProgress: false,
              skeleton: <ProgressSkeleton />,
            }}
          >
            <ResponseSkeleton />
          </StateSkeletons>
        </StateSkeletons>
      )

      expect(getByText("success")).toBeInTheDocument()
      expect(getByText("foo")).toBeInTheDocument()
      expect(queryByText("not rendered")).not.toBeInTheDocument()
    })

    it("should provide error via context hooks and work nested", () => {
      const { getByText, queryByText } = render(
        <StateSkeletons response="success">
          <ResponseSkeleton />
          <StateSkeletons
            errorConfig={{ error: "error1", skeleton: <ErrorSkeleton /> }}
          >
            <div>not rendered</div>
          </StateSkeletons>
        </StateSkeletons>
      )

      expect(getByText("success")).toBeInTheDocument()
      expect(getByText("error1")).toBeInTheDocument()
      expect(queryByText("not rendered")).not.toBeInTheDocument()
    })

    it("should provide inProgress via context hooks and work nested and render skeleton if no response is present", () => {
      const { getByText, queryByText } = render(
        <StateSkeletons response={true}>
          <StateSkeletons skeleton={<div>success</div>}>
            <div>not rendered</div>
          </StateSkeletons>
          <StateSkeletons
            progressConfig={{
              inProgress: true,
              skeleton: <ProgressSkeleton />,
            }}
          >
            <div>not rendered</div>
          </StateSkeletons>
        </StateSkeletons>
      )

      expect(getByText("success")).toBeInTheDocument()
      expect(getByText("yes")).toBeInTheDocument()
      expect(queryByText("not rendered")).not.toBeInTheDocument()
    })
  })
})
