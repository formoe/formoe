// @flow
import ACW from "./AsyncComponentWrapper"
import StSk, {
  useError as useE,
  useInProgress as useIP,
  useResponse as useR,
} from "./StateSkeletons"
import type { Params, Result, Return } from "./index.js.flow"

import { useCallback, useEffect, useState } from "react"

export default function useAsync<RequestType, ResultType, ErrorType>({
  asyncFunction,
  initialRequest,
}: Params<RequestType, ResultType>): Return<
  RequestType,
  ResultType,
  ErrorType
> {
  const [request, setRequest] = useState<RequestType | void>(initialRequest)
  const [result, setResult] = useState<
    Result<RequestType, ResultType, ErrorType>
  >({ request, inProgress: initialRequest !== undefined })

  useEffect(() => {
    let canceled = false
    const doAsync = async (request: RequestType) => {
      let asyncResponse
      try {
        asyncResponse = { response: await asyncFunction(request) }
      } catch (error) {
        asyncResponse = {
          error,
        }
      }
      if (!canceled) {
        setResult({ request, inProgress: false, ...asyncResponse })
        setRequest(undefined)
      }
    }

    if (!canceled && typeof request !== "undefined") {
      setResult({ request, inProgress: true })
      doAsync(request)
    }

    return () => {
      canceled = true
    }
  }, [asyncFunction, request])

  const reset = useCallback(() => {
    setRequest()
    setResult((currentResult) =>
      currentResult.request === undefined
        ? currentResult
        : { request: undefined, inProgress: false }
    )
  }, [])

  return { result, setRequest, reset }
}

export const AsyncComponentWrapper = ACW

export const StateSkeletons = StSk

export const useError = useE
export const useInProgress = useIP
export const useResponse = useR
