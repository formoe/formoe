// @flow
import * as React from "react"
import { useEffect } from "react"
import type { Props } from "./index.js.flow"
import useAsync from "./index"
import StateSkeletons from "./StateSkeletons"

export default function AsyncComponentWrapper<
  RequestType,
  ResponseType,
  ErrorType
>({
  children,
  asyncFunction,
  request,
  skeleton,
  progressSkeleton,
  errorSkeleton,
  onSuccess,
  onError,
  onProgressChange,
}: Props<RequestType, ResponseType, ErrorType>): React.Node {
  const { result, setRequest } = useAsync({
    asyncFunction,
    initialRequest: request,
  })

  useEffect(() => {
    setRequest(request)
  }, [request, setRequest])
  useEffect(() => {
    if (onProgressChange) {
      onProgressChange(result.inProgress)
    }
  }, [result.inProgress, onProgressChange])
  useEffect(() => {
    if (result.error && onError) {
      onError(result)
    }
  }, [result, onError])
  useEffect(() => {
    if (result.response && onSuccess) {
      onSuccess(result)
    }
  }, [result, onSuccess])

  return (
    <StateSkeletons
      response={result.response}
      skeleton={skeleton}
      errorConfig={
        errorSkeleton
          ? { skeleton: errorSkeleton, error: result.error }
          : undefined
      }
      progressConfig={
        progressSkeleton
          ? { skeleton: progressSkeleton, inProgress: result.inProgress }
          : undefined
      }
    >
      {children}
    </StateSkeletons>
  )
}
