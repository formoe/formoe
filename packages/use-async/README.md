# use-async

Provides functionality to simplify async operations in the UI.

## Hook

You can use the hook directly like this

```JS
import useAsync from "@formoe/use-async"

const asyncFunction = async (incomingRequest) => {
  // ... do something async here with the incoming request:
  // { body: "Foo" }
  return "Yay"
}

const initialRequest = {
  body: "Foo"
}

const { result, inProgress, setRequest } = useAsync({ asyncFunction, initialRequest })
```

`initialRequest` can be ommitted which results in the hook not making an initial request. If you need to make an initial call without a request, you can hand in `null`. This will also be given to your async function but you can choose to ignore it of course.

the interface is as follows:

```JS
{
  result: {
    request, // carries the request with wich the result was received => for the above code: { body: "Foo }
    response, // the return value of the async function if it succeeds (undefined on error) => for the above code: "Yay"
    error, // the error if the async function throws / rejects (undefined on success)
  },
  inProgress, // a boolean indicating whether or not the async process is running
  setRequest, // trigger a new request
}
```

with `setRequest` you can start a new request or retrigger the old one.

```JS
const newRequest = {
  body: "Bar"
}

setRequest(newRequest)
```

## Components

### StateSkeletons

This helper component provides a simple way to react to the state of your async process. Simply hand in the result and inProgress values from the hook and the corresponding skeletons are rendered:

```JSX
import { StateSkeletons } from "@formoe/use-async"

    <StateSkeletons
      response={result.response}}
      skeleton={<div>not initialized</div>}
      errorConfig={error: result.error, skeleton: <div>error</div>}
      progressConfig={inProgress, skeleton: <div>in progress</div>}
    >
      <div>success</div>
    </StateSkeletons>

```

- `children`: the component tree to render on success
- `errorConfig` (optional): an object with a component tree (skeleton) to render if an error occurs during the async process (error property of the config is truthy). If not provided the skeleton is rendered and your component does not provide visual feedback to errors.
- `skeleton` (optional): a component tree to render if the async process didn't run yet. If not provided the next valid skeleton is rendered and your component has to cope with and an uninitialized state. This skeleton should indicate loading.
- `progressConfig` (optional): an object with a component tree (skeleton) to render while async process is running (inProgress property of the config is truthy). If not provided children are rendered and your component does not provide visual feedback for the async operation.

In addition the components inside the skeletons can access the corresponmding state with the `useResponse`, `useError` and `useInProgress` hooks via the context wrapped around them by the `StateSkeletons` component.
For examples on how to use this [refer to the tests](src/index.spec.js).

### AsyncComponentWrapper

If you don't need to react to the response of the result but only render different things depending on it's state (for example disabling a button on posting form values), you can use this wrapper. The results are avaiable through call backs if you need to evaluate them.

Also as the wrapper uses `StateSkeletons`, components inside the tree can use the hooks described there to access the state.

```JSX
import { AsyncComponentWrapper } from "@formoe/use-async"

const asyncFunction = async (incomingRequest) => {
  // ... do something async here with the incoming request:
  // { body: "Foo" }
  return "Yay"
}

const request = {
  body: "Foo"
}

<AsyncComponentWrapper asyncFunction={asyncFunction} request={request}>
  <div>success</div>
</AsyncComponentWrapper>
```

The component provides the following property interface:

- `asyncFunction`: an async function (see hook)
- `request` (optional): the request send to the async function (see hook), nothing will happen unless set
- `onSuccess` (optional): a success callback called with the complete result on success.
- `onError` (optional): an error callback called with the complete result on error. If not provided the children are rendered.
- `onProgressChange` (optional): a callback triggered when the progress state of the async operation changes.
- skeletons work the same way as for the `StateSkeletons` above