# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.3.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@2.3.0...@formoe/use-async@2.3.1) (2022-09-21)


### Bug Fixes

* **use-async:** path to types file in package.json ([45f49aa](https://gitlab.com/formoe/formoe/commit/45f49aaf49220f0f765acdc62621cbaadc11d4d9))





# [2.3.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@2.2.0...@formoe/use-async@2.3.0) (2022-09-19)


### Features

* **use-async:** complete TS types ([efa70b9](https://gitlab.com/formoe/formoe/commit/efa70b964cf3d79636265322f1affd7173244cb5))





# [2.2.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@2.1.0...@formoe/use-async@2.2.0) (2022-09-19)


### Features

* **use-async:** enhance TS types ([b0fd99c](https://gitlab.com/formoe/formoe/commit/b0fd99c398ee01bd6e526fd71e4878c9f80f09ff))





# [2.1.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@2.0.1...@formoe/use-async@2.1.0) (2022-09-13)


### Features

* **use-async:** ad TypeScript types for hook ([cd87ca2](https://gitlab.com/formoe/formoe/commit/cd87ca2f9ad1b44805b80e348f1c0dea76bfb926))





## [2.0.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@2.0.0...@formoe/use-async@2.0.1) (2021-03-17)

**Note:** Version bump only for package @formoe/use-async





# [2.0.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@1.1.2...@formoe/use-async@2.0.0) (2020-11-18)


### Features

* **use-async:** incorporate in progress in returned result ([5559094](https://gitlab.com/formoe/formoe/commit/55590948e2058c539991cd93459f2ebfeef03176))


### BREAKING CHANGES

* **use-async:** The interface returned by the hook changed, inProgress is no longer available.





## [1.1.2](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@1.1.1...@formoe/use-async@1.1.2) (2020-11-17)

**Note:** Version bump only for package @formoe/use-async





## [1.1.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@1.1.0...@formoe/use-async@1.1.1) (2020-11-17)


### Bug Fixes

* **use-async:** prevent creation of new object on reset ([9345b1d](https://gitlab.com/formoe/formoe/commit/9345b1d94f2c1f413e70c86a39cff2c5c2710206))





# [1.1.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@1.0.2...@formoe/use-async@1.1.0) (2020-11-10)


### Features

* **use-async:** add reset function ([90e3af1](https://gitlab.com/formoe/formoe/commit/90e3af15b3f0ecad32f8e78a43a9dc23944a7bab))





## [1.0.2](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@1.0.1...@formoe/use-async@1.0.2) (2020-10-02)

**Note:** Version bump only for package @formoe/use-async





## [1.0.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-async@1.0.0...@formoe/use-async@1.0.1) (2020-09-30)

**Note:** Version bump only for package @formoe/use-async





# 1.0.0 (2020-09-11)


### Continuous Integration

* **use-async:** move to formoe registry ([eaaf07b](https://gitlab.com/jones-av/commons/commit/eaaf07bb632c3c93b0b03c91622b83f1e0eeb1fb))


### Features

* **use-async:** add state contexts for StateSkeeltons ([0fef828](https://gitlab.com/jones-av/commons/commit/0fef828a83820868c03be5662ee65bb8c17d7a07))
* **use-async:** add StateSkeletons component ([fab12e3](https://gitlab.com/jones-av/commons/commit/fab12e33241b4da423dc85875e400c72252e695b))


### BREAKING CHANGES

* **use-async:** Publishing to different registry basically introducing a completely new package
* **use-async:** StateSkeletons interface restructured
