# use-interval

You can use the hook like this

```JS
import useAsync from "@formoe/use-interval"

const doSomething = () => {
  console.log("Yay")
}

// inside a component or hook ...

const { start, stop, running } = useInterval({ initialInterval: 0, callBack: doSomething })

// an effect that triggers the start of the interval (could also be a callback)
useEffect(() => start, [])

// a callback to stop the interval to hand outside (could also be an effect)
const stopTheHook = useCallback(() => stop, [])
```

`initialInterval` the interval in ms when the callback should be triggered, you can hand in `0` to prevent it from running immediately.
