// @flow
import { renderHook, act } from "@testing-library/react-hooks"
import useInterval from "./index"
import { waitForMillis } from "../../../test/test-utils"

describe("useInterval", () => {
  it("should call the callback in the given interval", async () => {
    const callBack = jest.fn()

    const { result: hookInterface } = renderHook(() =>
      useInterval({ initialInterval: 50, callBack })
    )

    let hook = hookInterface.current
    expect(hook.running).toBe(true)
    await waitForMillis(180)
    hook = hookInterface.current
    expect(hook.running).toBe(true)
    act(() => {
      hook.stop()
    })
    // ensure nothing is called after stop
    await waitForMillis(100)
    hook = hookInterface.current
    expect(hook.running).toBe(false)
    expect(callBack).toHaveBeenCalledTimes(3)

    callBack.mockClear()

    // start again with shorter interval
    act(() => {
      hook.start(20)
    })
    hook = hookInterface.current
    expect(hook.running).toBe(true)
    await waitForMillis(50)
    expect(hook.running).toBe(true)
    expect(callBack).toHaveBeenCalledTimes(2)
    act(() => {
      hook.stop()
    })

    callBack.mockClear()

    // start again with initial interval
    act(() => {
      hook.start()
    })
    hook = hookInterface.current
    expect(hook.running).toBe(true)
    await waitForMillis(80)
    expect(hook.running).toBe(true)
    expect(callBack).toHaveBeenCalledTimes(1)
    act(() => {
      hook.stop()
    })
  })
})
