// @flow
import { useState, useEffect, useCallback } from "react"
import type { Params, Interface } from "./index.js.flow"

export default ({ initialInterval, callBack }: Params): Interface => {
  const [interval, setIntervalTime] = useState(initialInterval)
  const [running, setRunning] = useState(initialInterval > 0)

  useEffect(() => {
    if (interval > 0) {
      const id = setInterval(callBack, interval)
      return () => clearInterval(id)
    }
  }, [callBack, interval])

  useEffect(() => {
    setRunning(interval > 0)
  }, [interval])

  const start = useCallback(
    (interval = initialInterval) => {
      setIntervalTime(interval)
    },
    [initialInterval]
  )
  const stop = useCallback(() => {
    setIntervalTime(0)
  }, [])
  return { running, start, stop }
}
