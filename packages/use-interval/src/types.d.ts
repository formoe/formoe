declare module "@formoe/use-interval" {
  interface Params {
    initialInterval: number
    callBack: (timestamp?: number) => void
  }

  interface Interface {
    start: (interval: number) => void
    stop: () => void
    running: boolean
  }
  
  function useInterval(params: Params): Interface

  export default useInterval
}