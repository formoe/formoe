# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.2.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-interval@1.1.0...@formoe/use-interval@1.2.0) (2022-09-21)


### Bug Fixes

* **use-interval:** path to types file in package.json ([e019290](https://gitlab.com/formoe/formoe/commit/e019290dcb4f1bbf61384ce78b00f13ecbf1a059))


### Features

* **use-interval:** update docs ([fb00aec](https://gitlab.com/formoe/formoe/commit/fb00aec7e10a8506e2540e36c1eed072e7ab86b1))





# [1.1.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-interval@1.0.4...@formoe/use-interval@1.1.0) (2022-09-13)


### Features

* **use-interval:** add typescript types ([346801b](https://gitlab.com/formoe/formoe/commit/346801b3fea568152efeb85f916189a2bd12f47e))





## [1.0.4](https://gitlab.com/formoe/formoe/compare/@formoe/use-interval@1.0.3...@formoe/use-interval@1.0.4) (2021-03-17)

**Note:** Version bump only for package @formoe/use-interval





## [1.0.3](https://gitlab.com/formoe/formoe/compare/@formoe/use-interval@1.0.2...@formoe/use-interval@1.0.3) (2020-11-17)


### Bug Fixes

* **use-interval:** prevent unwanted hook changes ([451c462](https://gitlab.com/formoe/formoe/commit/451c46262e8c2e60c0ff6710490fe97a5262d080))





## [1.0.2](https://gitlab.com/formoe/formoe/compare/@formoe/use-interval@1.0.1...@formoe/use-interval@1.0.2) (2020-10-02)

**Note:** Version bump only for package @formoe/use-interval





## [1.0.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-interval@1.0.0...@formoe/use-interval@1.0.1) (2020-09-30)

**Note:** Version bump only for package @formoe/use-interval





# 1.0.0 (2020-09-11)


### Continuous Integration

* **use-interval:** move to formoe registry ([3a20933](https://gitlab.com/jones-av/commons/commit/3a209334342d15cc5cadae1d662ba644aefd6bb8))


### BREAKING CHANGES

* **use-interval:** Publishing to different registry basically introducing a completely new package
