# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [2.1.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-selection@2.0.1...@formoe/use-selection@2.1.0) (2022-12-22)


### Features

* **use-selection:** typescript types ([fc10e27](https://gitlab.com/formoe/formoe/commit/fc10e27a0c2d9d81ee25281bc5415bdacb5e7814))





## [2.0.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-selection@2.0.0...@formoe/use-selection@2.0.1) (2021-11-11)

**Note:** Version bump only for package @formoe/use-selection





# [2.0.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-selection@1.1.0...@formoe/use-selection@2.0.0) (2021-06-28)


### Features

* **use-selection:** add multi selection capabilities ([6222fe1](https://gitlab.com/formoe/formoe/commit/6222fe1c39f3646ff4a4c82b2776097a5b319c7e))
* **use-selection:** make contextId optional ([29b446c](https://gitlab.com/formoe/formoe/commit/29b446c7304ed8bc9855c5514ca9f9597db197f4))


### BREAKING CHANGES

* **use-selection:** The contextId is now mandatory





# [1.1.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-selection@1.0.1...@formoe/use-selection@1.1.0) (2021-05-27)


### Features

* **use-selection:** add option to deselect item ([e9027bf](https://gitlab.com/formoe/formoe/commit/e9027bfb38c1439308bb5925abbb4b12b866a3a3))





## 1.0.1 (2021-05-27)

**Note:** Version bump only for package @formoe/use-selection
