declare module "@formoe/use-selection" {
  interface Props<Item> extends React.PropsWithChildren {
    options: Item[]
    contextId?: string
  }

  type Interface<Item> = {
    options: Item[]
    selectedItem?: Item
    selectItem: (item: Item | void) => void
  }

  export function SelectionProvider<Item>(props: Props<Item>): JSX.Element
  export function useSelection<Item>(contextId?: string): Interface<Item>

  export const DEFAULT_CONTEXT: "1"
}