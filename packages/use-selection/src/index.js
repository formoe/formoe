// @flow
import React, {
  useState,
  createContext,
  useContext,
  useCallback,
  useEffect,
  useRef,
} from "react"
import type { Node } from "react"

import type { Params, Interface } from "./index.js.flow"

export const DEFAULT_CONTEXT = "1"

// Be aware, that typings do not work sufficiently.
// We can't provide the context with the real generic typing, as it not known here.
// This leads to a break between handing in options to the provider
// and retrieving them with the hook.
// Ensure correct typing by handing in the type from the outside, matching the options against it and
// typing the hook accordingly.
// See spec on how this can be achieved.
const contextMap = new Map<string, React$Context<any>>()

export const SelectionProvider = <Item>({
  children,
  options,
  contextId = DEFAULT_CONTEXT,
}: Params<Item>): Node => {
  const [selectedItem, setSelectedItem] = useState<Item | void>()

  let SelectionContext = useRef(createContext<Interface<Item> | void>())

  contextMap.set(contextId, SelectionContext.current)
  useEffect(() => {
    return () => {
      contextMap.delete(contextId)
    }
  }, [contextId])

  const selectItem = useCallback(
    (item2Select) => {
      if (typeof item2Select === "undefined" || options.includes(item2Select)) {
        setSelectedItem(item2Select)
      }
    },
    [options]
  )

  return (
    <SelectionContext.current.Provider
      value={{ selectedItem, selectItem, options }}
    >
      {children}
    </SelectionContext.current.Provider>
  )
}

export const useSelection = <Item>(
  contextId: string = "1"
): Interface<Item> => {
  const context = contextMap.get(contextId)

  if (!context)
    throw `Context with id "${contextId}" not found. Did you use the wrong contextId or forget to wrap your components in the SelectionProvider?`

  return useContext(context)
}
