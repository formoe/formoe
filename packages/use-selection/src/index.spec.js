// @flow
import React, { useState } from "react"
import { render, within } from "@testing-library/react"
import userEvent from "@testing-library/user-event"

import { useSelection, SelectionProvider } from "./index"

type ItemType = {|
  id: string,
  name: string,
|}

const TestingComponent = ({ contextId }: { contextId?: string }) => {
  const { options, selectedItem, selectItem } = useSelection<ItemType>(
    contextId
  )
  return (
    <div data-testid={contextId}>
      {options.map((item) => (
        <button onClick={() => selectItem(item)} key={item.id}>
          Select: {item.id} - {item.name}
        </button>
      ))}
      {selectedItem && (
        <h1>
          Selected Item: {selectedItem.id} - {selectedItem.name}
        </h1>
      )}
      <button onClick={() => selectItem()}>Deselect</button>
      <button onClick={() => selectItem({ id: "NONE", name: "I'm not there" })}>
        Select non-existent
      </button>
    </div>
  )
}

describe("useSelection", () => {
  it("should throw with an interpretable error when not wrapped in provider", () => {
    expect(() => render(<TestingComponent />)).toThrow(
      'Context with id "1" not found. Did you use the wrong contextId or forget to wrap your components in the SelectionProvider?'
    )
  })

  it("should throw with an interpretable error when the wron context id is used", () => {
    const options = []

    expect(() =>
      render(
        <SelectionProvider options={options}>
          <TestingComponent contextId="foo" />
        </SelectionProvider>
      )
    ).toThrow(
      'Context with id "foo" not found. Did you use the wrong contextId or forget to wrap your components in the SelectionProvider?'
    )
  })

  describe("when no options are provided", () => {
    it("should return no selected item intially", () => {
      const options = []
      const { queryByRole, queryAllByRole } = render(
        <SelectionProvider options={options}>
          <TestingComponent />
        </SelectionProvider>
      )
      expect(queryByRole("heading")).toBeNull()
      // only the deselect and select none button exist
      expect(queryAllByRole("button")).toHaveLength(2)
    })
  })

  describe("when options are provided", () => {
    let r

    beforeEach(() => {
      const options: ItemType[] = [
        { id: "1", name: "first" },
        { id: "2", name: "second" },
        { id: "3", name: "third" },
      ]
      r = render(
        <SelectionProvider options={options}>
          <TestingComponent />
        </SelectionProvider>
      )
    })

    it("should return no selected item intially", () => {
      const { queryByRole, queryAllByRole, getByRole } = r
      expect(queryByRole("heading")).toBeNull()
      expect(queryAllByRole("button")).toHaveLength(5)
      getByRole("button", { name: "Select: 1 - first" })
      getByRole("button", { name: "Select: 2 - second" })
      getByRole("button", { name: "Select: 3 - third" })
    })

    it("should return the selected item after selecting it", () => {
      const { getByRole } = r
      userEvent.click(getByRole("button", { name: "Select: 2 - second" }))
      getByRole("heading", { name: "Selected Item: 2 - second" })
    })

    it("should unselect an item when calling select without arguments", () => {
      const { getByRole, queryByRole } = r
      userEvent.click(getByRole("button", { name: "Select: 2 - second" }))
      getByRole("heading", { name: "Selected Item: 2 - second" })
      userEvent.click(getByRole("button", { name: "Deselect" }))
      expect(queryByRole("heading")).toBeNull()
    })

    it("should not change the selected item if something is selected that doesn't exist", () => {
      const { getByRole, queryByRole } = r
      expect(queryByRole("heading")).toBeNull()
      userEvent.click(getByRole("button", { name: "Select non-existent" }))
      expect(queryByRole("heading")).toBeNull()
      userEvent.click(getByRole("button", { name: "Select: 1 - first" }))
      getByRole("heading", { name: "Selected Item: 1 - first" })
      userEvent.click(getByRole("button", { name: "Select non-existent" }))
      getByRole("heading", { name: "Selected Item: 1 - first" })
    })
  })

  describe("when working with multiple context providers", () => {
    let r

    const DoubleProvider = () => {
      const [optionsFoo, setOptionsFoo] = useState<ItemType[]>([
        { id: "foo1", name: "fooFirst" },
        { id: "2", name: "second" },
      ])

      const optionsBar: ItemType[] = [
        { id: "bar1", name: "barFirst" },
        { id: "2", name: "second" },
      ]

      return (
        <>
          <button
            onClick={() =>
              setOptionsFoo((currentOptions) => [
                ...currentOptions,
                {
                  id: (currentOptions.length + 1).toString(),
                  name: (currentOptions.length + 1).toString(),
                },
              ])
            }
          >
            Add foo option
          </button>
          <SelectionProvider options={optionsFoo} contextId="foo">
            <SelectionProvider options={optionsBar} contextId="bar">
              <TestingComponent contextId="foo" />
              <TestingComponent contextId="bar" />
            </SelectionProvider>
          </SelectionProvider>
        </>
      )
    }

    beforeEach(() => {
      r = render(<DoubleProvider />)
    })

    it("should cope with rerenders", () => {
      const { getByTestId, getByRole } = r
      let foo = within(getByTestId("foo"))
      let bar = within(getByTestId("bar"))
      expect(foo.queryByRole("heading")).toBeNull()
      expect(bar.queryByRole("heading")).toBeNull()

      // add another option to foo => triggers rerender
      userEvent.click(getByRole("button", { name: "Add foo option" }))
      expect(foo.queryByRole("heading")).toBeNull()
      expect(bar.queryByRole("heading")).toBeNull()

      // select new option...
      userEvent.click(foo.getByRole("button", { name: "Select: 3 - 3" }))
      foo.getByRole("heading", { name: "Selected Item: 3 - 3" })
      expect(bar.queryByRole("heading")).toBeNull()
      // ... then rerender and check if selection is still correct
      userEvent.click(getByRole("button", { name: "Add foo option" }))
      foo.getByRole("heading", { name: "Selected Item: 3 - 3" })
    })

    it("should only select items of the correct context", () => {
      const { getByTestId } = r
      const foo = within(getByTestId("foo"))
      const bar = within(getByTestId("bar"))
      userEvent.click(foo.getByRole("button", { name: "Select: 2 - second" }))
      foo.getByRole("heading", { name: "Selected Item: 2 - second" })
      expect(bar.queryByRole("heading")).toBeNull()
    })
  })
})
