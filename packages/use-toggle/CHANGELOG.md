# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.1.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-toggle@2.1.0...@formoe/use-toggle@2.1.1) (2022-12-22)


### Bug Fixes

* **use-toggle:** try to downgrade to react 16 again ([e6b6c67](https://gitlab.com/formoe/formoe/commit/e6b6c67327d62b369cd03f1de7f205f6764f413c))





# [2.1.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-toggle@2.0.0...@formoe/use-toggle@2.1.0) (2022-12-22)


### Features

* **use-toggle:** typescript types ([0f28513](https://gitlab.com/formoe/formoe/commit/0f285135070bc7ee20b6076008a0631e5a685cb7))





# [2.0.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-toggle@1.0.3...@formoe/use-toggle@2.0.0) (2021-11-11)


### Build System

* **use-toggle:** upgrade to react 17 and switch to peer dependencies ([2b0385c](https://gitlab.com/formoe/formoe/commit/2b0385c306bb7ca8875006ac14b1971c82168de0))


### BREAKING CHANGES

* **use-toggle:** React is now a peer dependency and needs to be upgraded to 17





## [1.0.3](https://gitlab.com/formoe/formoe/compare/@formoe/use-toggle@1.0.2...@formoe/use-toggle@1.0.3) (2021-03-17)

**Note:** Version bump only for package @formoe/use-toggle





## [1.0.2](https://gitlab.com/formoe/formoe/compare/@formoe/use-toggle@1.0.1...@formoe/use-toggle@1.0.2) (2020-10-02)

**Note:** Version bump only for package @formoe/use-toggle





## [1.0.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-toggle@1.0.0...@formoe/use-toggle@1.0.1) (2020-09-30)

**Note:** Version bump only for package @formoe/use-toggle





# 1.0.0 (2020-09-11)


### Continuous Integration

* **use-toggle:** move to formoe registry ([e407273](https://gitlab.com/jones-av/commons/commit/e407273f6b6b5cc0ecc0b696138b79863dc6717a))


### BREAKING CHANGES

* **use-toggle:** Publishing to different registry basically introducing a completely new package
