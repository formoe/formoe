declare module "@formoe/use-toggle" {
  export type Interface = [boolean, (state: boolean | void) => void]

  export default function useToggle(initialState?: boolean): Interface
}