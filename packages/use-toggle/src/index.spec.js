// @flow
import { renderHook, act } from "@testing-library/react-hooks"
import useToggle from "./index"

describe("use-toggle", () => {
  it("should initialize with false per default", () => {
    const { result } = renderHook(() => useToggle())

    const [value] = result.current
    expect(value).toBe(false)
  })

  it("should initialize with true if set", () => {
    const { result } = renderHook(() => useToggle(true))
    const [value] = result.current
    expect(value).toBe(true)
  })

  it("should toggle value", () => {
    const { result } = renderHook(() => useToggle())

    act(() => {
      const toggleValue = result.current[1]
      toggleValue()
    })

    const [value] = result.current
    expect(value).toBe(true)

    act(() => {
      const toggleValue = result.current[1]
      toggleValue()
    })

    const [nextValue] = result.current
    expect(nextValue).toBe(false)
  })

  it("should set the value if toggle functions receives a value", () => {
    const { result } = renderHook(() => useToggle())

    act(() => {
      const toggleValue = result.current[1]
      toggleValue(false)
    })

    const [value] = result.current
    expect(value).toBe(false)

    act(() => {
      const toggleValue = result.current[1]
      toggleValue(true)
    })

    const [nextValue] = result.current
    expect(nextValue).toBe(true)
  })
})
