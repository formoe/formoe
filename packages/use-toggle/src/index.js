// @flow
import { useReducer } from "react"
import type { Interface } from "./index.js.flow"

/**
 * Simple hook wrapper, providing toggle behaviour.
 * @param {boolean} [initialState=false] intially toggled on or off? Default: off
 * @returns {Array} Array with interface: first entry is the on / off state, second entry is the toggle method to switch the state
 */
export default (initialState?: boolean = false): Interface => {
  const reducerInterface = useReducer<boolean, boolean | void>(
    (isOn, setOn) => (typeof setOn === "boolean" ? setOn : !isOn),
    initialState
  )

  return reducerInterface
}
