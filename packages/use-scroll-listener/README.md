# useScrollListener

A simple react hook registering and unregistering a window scroll listener.

## Usage

```JavaScript
const handleScroll = () => {
  // what shall we do with the drunken scroller…
}

useScrollListener({ handleScroll })
```
