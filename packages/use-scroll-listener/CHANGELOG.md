# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://gitlab.com/formoe/formoe/compare/@formoe/use-scroll-listener@1.0.2...@formoe/use-scroll-listener@1.0.3) (2021-03-17)

**Note:** Version bump only for package @formoe/use-scroll-listener





## [1.0.2](https://gitlab.com/formoe/formoe/compare/@formoe/use-scroll-listener@1.0.1...@formoe/use-scroll-listener@1.0.2) (2020-10-02)

**Note:** Version bump only for package @formoe/use-scroll-listener





## [1.0.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-scroll-listener@1.0.0...@formoe/use-scroll-listener@1.0.1) (2020-09-30)

**Note:** Version bump only for package @formoe/use-scroll-listener





# 1.0.0 (2020-09-11)


### Continuous Integration

* **use-scroll-listener:** move to formoe registry ([92fc194](https://gitlab.com/jones-av/commons/commit/92fc194c04264b9c56f3f586e53135a361c723aa))


### BREAKING CHANGES

* **use-scroll-listener:** Publishing to different registry basically introducing a completely new package
