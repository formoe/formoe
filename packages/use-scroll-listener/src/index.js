// @flow
import { useLayoutEffect } from "react"
import type { ScrollHandler } from "./index.js.flow"

export default ({ handleScroll }: ScrollHandler) => {
  useLayoutEffect(() => {
    window.addEventListener("scroll", handleScroll, { passive: true })

    return () => window.removeEventListener("scroll", handleScroll)
  }, [handleScroll])
}
