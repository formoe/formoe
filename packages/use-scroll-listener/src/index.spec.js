// @flow
import React from "react"
import useScrollListener from "./index"
import { render } from "@testing-library/react"

const DummyComponent = () => {
  useScrollListener({ handleScroll: () => {} })
  return <h1>Success</h1>
}

describe("useScrollListener", () => {
  it("should not throw errors when used", () => {
    const { getByRole } = render(<DummyComponent />)
    expect(getByRole("heading", { name: "Success" })).not.toBeNull()
  })
})
