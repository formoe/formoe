# use-multi-async

Provides a hook to simplify multiple async operations in the UI.

## Hook

```JS
import useMultiAsync from "@formoe/use-multi-async"

const asyncFunction = async (incomingRequest) => {
  // ... do something async here with the incoming request:
  // { search: "Foo" }
  return "Yay"
}

const { results, inProgress, startRequest, getResult, clearResults, cancelAllRequests, cancelRequest, reset } = useMultiAsync({ asyncFunction, maxResults: 10 })

const reqId = startRequest({ search: "Foo" })

useEffect(() => {
  const result = getResult(reqId)
  if (result) {
    // do something with the result, here "Yay"
  }
}, [results])

```

The hook configuration takes an optional `maxResults` property. If set the hook will dismiss `results` after reaching the maximum configured number with a result que (fifo). This is usefull to safe memory when starting massive amounts of requests. Keep in mind though, that this is not exact, as it uses react state effects, so the amount of results might exceed `maxResults` at times.
If omitted, all results are kept.

The returned interface is as follows:

```JS
{
  startRequest, // function: to trigger a new request
  results: // an array of all results for finished requests
  [{
    request: {
      id, // the id returned by startRequest
      body, // carries the request body that started the operation => for the above code: { search: "Foo" }
    },
    response, // the return value of the async function if it succeeds (undefined on error) => for the above code: "Yay"
    error, // the error if the async function throws / rejects (undefined on success)
  }],
  getResult, // function: to retreive a result for the operation with the given id
  inProgress, // a boolean indicating whether or not any async process is running
  clearResults, // clear all retreived results
  cancelAllRequests, // cancel all running requests
  cancelRequest, // cancel a specific request
  reset, // reset the complete state canceling requests and clearing results
}
```

With `startRequest` you can start a new request. It returns a `Symbol` that can be used to find the request in the results array as soon as it was finished. You can start multiple requests at once and `inProgress` will be true as long as any request is running. To check whether a specific request runs, try to find it via `getResult`. If it's not there, it's still running.

```JS
const id1 = startRequest({
  body: "Foo"
})

const id2 = startRequest({
  body: "Bar"
})

useEffect(() => {
  const res1 = getResult(id1)
  const res2 = getResult(id2)
  if (res1) {
    console.log(res1.response)
  }
  if (res2) {
    console.log(res2.response)
  }
}, [results])
```

To control the state of results and requests from outside the hook provides various functions:

Extending on the example above, you could:

- clear all so far retreived results

```JS

clearResults()

```

- cancel a specific request

```JS

cancelRequest(id1)

```

- cancel all currently running requests

```JS

cancelAllRequests()

```

- or reset the complete hook by canceling running requests and clearing all results:

```JS

reset()

```

Keep in mind, that the hook can not really abort requests. It only dismisses returning requests that were canceled. Effects of the async processes (like POSTs to a backend system) will still apply.
