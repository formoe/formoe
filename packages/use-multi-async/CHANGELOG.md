# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 2.2.0 (2023-03-17)


### Features

* **use-media-stream:** introduce hook to acquire meda streams ([3410561](https://gitlab.com/formoe/formoe/commit/34105612d6b42e39b384a63a83bf1adaaaa4a274))
* **use-multi-async:** added types ([921067c](https://gitlab.com/formoe/formoe/commit/921067c7dc2ad7129348ca0f3d5146ed134f7943))





## [2.1.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-multi-async@2.1.0...@formoe/use-multi-async@2.1.1) (2021-03-17)


### Bug Fixes

* **use-multi-async:** simplify request triggering to prevent false calls ([2fe8b89](https://gitlab.com/formoe/formoe/commit/2fe8b89e6aab21a3935d44d5ea9c784ee0edfefd))





# [2.1.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-multi-async@2.0.1...@formoe/use-multi-async@2.1.0) (2020-10-05)


### Features

* **use-multi-async:** add interface to control request execution ([4bd6ef5](https://gitlab.com/formoe/formoe/commit/4bd6ef56d7206483092491e55dec0b9332406b75))





## [2.0.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-multi-async@2.0.0...@formoe/use-multi-async@2.0.1) (2020-10-02)


### Bug Fixes

* **use-multi-async:** flow typings: request is mendatory in result ([a81b9af](https://gitlab.com/formoe/formoe/commit/a81b9af107914d5a978908470ed8e14d9079f183))





# 2.0.0 (2020-09-30)


### Features

* **use-multi-async:** introduce hook to trigger multiple async requests ([ced844a](https://gitlab.com/formoe/formoe/commit/ced844a4f4adebf119cd38ee02a31815eb1c20f1))


### BREAKING CHANGES

* **use-multi-async:** Introduction of new package
