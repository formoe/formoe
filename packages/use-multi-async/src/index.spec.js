// @flow
import { renderHook, act } from "@testing-library/react-hooks"

import { waitForMillis } from "../../../test/test-utils"

import useMultiAsync from "./index"
import type { AsyncFunction } from "./index"

type ResolveWithValue<ResultType> = {
  response: ResultType,
  respondInMillis?: number,
}

function asyncResponse<RequestType, ResultType>({
  respondInMillis,
  resolveWith,
  resolveWithMap,
  rejectWith,
}: {
  respondInMillis?: number,
  resolveWith?: ResultType,
  resolveWithMap?: Map<RequestType, ResolveWithValue<ResultType>>,
  rejectWith?: any,
}): AsyncFunction<RequestType, ResultType> {
  return jest.fn(async (request?: RequestType) => {
    if (respondInMillis) {
      // respondInMillis can be a simple number or a map of reqwuest to waiting time
      await waitForMillis(respondInMillis)
    }

    if (rejectWith) {
      return Promise.reject(rejectWith)
    }

    let result = resolveWith
    // do we have a request to response mapping?
    if (resolveWithMap && request) {
      const requestMapping = resolveWithMap.get(request)
      if (requestMapping) {
        result = requestMapping.response
        if (requestMapping.respondInMillis) {
          await waitForMillis(requestMapping.respondInMillis)
        }
      }
    }

    return (
      result ||
      Promise.reject(
        "Expected response not found, please define 'resolveWith' or rejectWith 'properties."
      )
    )
  })
}

describe("use-multi-async", () => {
  it("should be instantiable and provide defaults", async () => {
    const asyncFunction = jest.fn()
    const { result: hookInterface } = renderHook(() =>
      useMultiAsync({
        asyncFunction,
      })
    )

    const {
      results: initialResults,
      startRequest,
      getResult,
      inProgress,
    } = hookInterface.current
    expect(initialResults).toStrictEqual([])
    expect(asyncFunction).not.toHaveBeenCalledWith()
    expect(typeof startRequest).toBe("function")
    expect(typeof getResult).toBe("function")
    expect(inProgress).toBe(false)
  })

  it("should run a given request", async () => {
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 50,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useMultiAsync({
        asyncFunction,
      })
    )

    const { startRequest } = hookInterface.current

    let id
    await act(async () => {
      id = startRequest("1337 req")
    })

    const { results: startedResults } = hookInterface.current
    expect(startedResults).toStrictEqual([])
    // check we don't have excessive calls
    expect(asyncFunction).toHaveBeenCalledWith("1337 req")
    expect(asyncFunction).toHaveBeenCalledTimes(1)
    expect(hookInterface.current.getResult(id)).toBeUndefined()
    expect(hookInterface.current.inProgress).toBe(true)

    await waitForNextUpdate()

    const { results: finishedResults } = hookInterface.current
    expect(finishedResults).toEqual([
      {
        request: { id, body: "1337 req" },
        response: "1337",
      },
    ])
    expect(hookInterface.current.getResult(id)).toStrictEqual({
      request: { id, body: "1337 req" },
      response: "1337",
    })
    expect(hookInterface.current.inProgress).toBe(false)
  })

  it("should show request errors", async () => {
    const asyncFunction = asyncResponse({
      rejectWith: "7353",
      respondInMillis: 50,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useMultiAsync({
        asyncFunction,
      })
    )

    const { startRequest } = hookInterface.current

    let id
    await act(async () => {
      id = startRequest("Error req")
    })
    expect(hookInterface.current.inProgress).toBe(true)

    await waitForNextUpdate()

    expect(hookInterface.current.getResult(id)).toStrictEqual({
      request: { id, body: "Error req" },
      error: "7353",
    })
    expect(hookInterface.current.inProgress).toBe(false)
  })

  it("should cope with unmounting", async () => {
    // eslint-disable-next-line no-undef
    const errorSpy = jest.spyOn(global.console, "error")
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 200,
    })
    const { result: hookInterface, unmount } = renderHook(() =>
      useMultiAsync({
        asyncFunction,
      })
    )

    const { startRequest } = hookInterface.current

    await act(async () => {
      startRequest("1337 req")
    })
    expect(hookInterface.current.inProgress).toBe(true)

    unmount()
    // we would get console errors if act would occur outside of test
    expect(errorSpy).not.toHaveBeenCalled()
  })

  it("should be able to trigger the same request twice in a row", async () => {
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 50,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useMultiAsync({
        asyncFunction,
      })
    )

    const { startRequest } = hookInterface.current

    let id1
    let id2
    await act(async () => {
      id1 = startRequest("1337 req")
      id2 = startRequest("1337 req")
    })

    const { results: startedResults } = hookInterface.current
    expect(startedResults).toStrictEqual([])
    expect(asyncFunction).toHaveBeenCalledWith("1337 req")
    expect(asyncFunction).toHaveBeenCalledTimes(2)

    await waitForNextUpdate()

    const { results: finishedResults } = hookInterface.current
    expect(finishedResults).toEqual([
      {
        request: { id: id1, body: "1337 req" },
        response: "1337",
      },
      {
        request: { id: id2, body: "1337 req" },
        response: "1337",
      },
    ])
    expect(hookInterface.current.getResult(id1)).toStrictEqual({
      request: { id: id1, body: "1337 req" },
      response: "1337",
    })
    expect(hookInterface.current.getResult(id2)).toStrictEqual({
      request: { id: id2, body: "1337 req" },
      response: "1337",
    })
  })

  it("should provide the option to clear results", async () => {
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 50,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useMultiAsync({
        asyncFunction,
      })
    )

    const { startRequest } = hookInterface.current

    let id1
    let id2
    await act(async () => {
      id1 = startRequest("1337 req")
      id2 = startRequest("1337 req")
    })

    const { results: startedResults } = hookInterface.current
    expect(startedResults).toStrictEqual([])
    expect(asyncFunction).toHaveBeenCalledWith("1337 req")
    expect(asyncFunction).toHaveBeenCalledTimes(2)

    await waitForNextUpdate()

    const { results: finishedResults, clearResults } = hookInterface.current
    expect(finishedResults).toEqual([
      {
        request: { id: id1, body: "1337 req" },
        response: "1337",
      },
      {
        request: { id: id2, body: "1337 req" },
        response: "1337",
      },
    ])
    expect(hookInterface.current.getResult(id1)).toStrictEqual({
      request: { id: id1, body: "1337 req" },
      response: "1337",
    })
    expect(hookInterface.current.getResult(id2)).toStrictEqual({
      request: { id: id2, body: "1337 req" },
      response: "1337",
    })

    act(() => {
      clearResults()
    })

    const { results: clearedResults } = hookInterface.current
    expect(clearedResults).toEqual([])
    expect(hookInterface.current.getResult(id1)).toBeUndefined()
    expect(hookInterface.current.getResult(id2)).toBeUndefined()
  })

  it("should only keep the specified amount of results", async () => {
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 10,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useMultiAsync({
        asyncFunction,
        maxResults: 2,
      })
    )

    const { startRequest } = hookInterface.current

    let id2
    let id3
    await act(async () => {
      startRequest("req 1")
      id2 = startRequest("req 2")
      id3 = startRequest("req 3")
    })

    const { results: startedResults } = hookInterface.current
    expect(startedResults).toStrictEqual([])

    await waitForNextUpdate()

    expect(asyncFunction).toHaveBeenCalledWith("req 1")
    expect(asyncFunction).toHaveBeenCalledWith("req 2")
    expect(asyncFunction).toHaveBeenCalledWith("req 3")
    expect(asyncFunction).toHaveBeenCalledTimes(3)

    const { results: finishedResults } = hookInterface.current
    expect(finishedResults).toEqual([
      {
        request: { id: id2, body: "req 2" },
        response: "1337",
      },
      {
        request: { id: id3, body: "req 3" },
        response: "1337",
      },
    ])
  })

  it("should be able to cancel specific requests", async () => {
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 10,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useMultiAsync({
        asyncFunction,
      })
    )

    const { startRequest } = hookInterface.current

    let id1
    let id2
    let id3
    await act(async () => {
      id1 = startRequest("req 1")
      id2 = startRequest("req 2")
      id3 = startRequest("req 3")
    })

    const { cancelRequest } = hookInterface.current

    await act(async () => {
      cancelRequest(id2)
      cancelRequest(id3)
    })

    await waitForNextUpdate()

    expect(asyncFunction).toHaveBeenCalledWith("req 1")
    expect(asyncFunction).toHaveBeenCalledWith("req 2")
    expect(asyncFunction).toHaveBeenCalledWith("req 3")
    expect(asyncFunction).toHaveBeenCalledTimes(3)

    const { results: finishedResults } = hookInterface.current
    expect(finishedResults).toEqual([
      {
        request: { id: id1, body: "req 1" },
        response: "1337",
      },
    ])
  })

  it("should be able to cancel all requests", async () => {
    // eslint-disable-next-line no-undef
    const errorSpy = jest.spyOn(global.console, "error")
    const asyncFunction = asyncResponse({
      resolveWith: "1337",
      respondInMillis: 100,
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useMultiAsync({
        asyncFunction,
      })
    )

    const { startRequest } = hookInterface.current

    await act(async () => {
      startRequest("req 1")
      startRequest("req 2")
      startRequest("req 3")
    })

    const { cancelAllRequests } = hookInterface.current

    await act(async () => {
      cancelAllRequests()
    })

    await waitForNextUpdate()

    expect(asyncFunction).toHaveBeenCalledWith("req 1")
    expect(asyncFunction).toHaveBeenCalledWith("req 2")
    expect(asyncFunction).toHaveBeenCalledWith("req 3")
    expect(asyncFunction).toHaveBeenCalledTimes(3)

    const { results: finishedResults } = hookInterface.current
    expect(finishedResults).toEqual([])

    // we would get console errors if act would occur outside of test
    expect(errorSpy).not.toHaveBeenCalled()
  })

  it("should stay inProgress as long as any request runs", async () => {
    const asyncFunction = asyncResponse({
      respondInMillis: 200,
      resolveWithMap: new Map([
        ["request 1", { response: "response 1" }],
        ["request 2", { response: "response 2" }],
        ["request 3", { response: "response 3" }],
      ]),
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useMultiAsync<string, string, string>({
        asyncFunction,
      })
    )

    let id1
    await act(async () => {
      id1 = hookInterface.current.startRequest("request 1")
    })

    expect(hookInterface.current.inProgress).toBe(true)

    await waitForMillis(10)

    let id2
    await act(async () => {
      id2 = hookInterface.current.startRequest("request 2")
    })
    expect(hookInterface.current.inProgress).toBe(true)

    await waitForMillis(10)

    let id3
    await act(async () => {
      id3 = hookInterface.current.startRequest("request 3")
    })
    expect(hookInterface.current.inProgress).toBe(true)

    await waitForNextUpdate()

    expect(hookInterface.current.getResult(id1)).toStrictEqual({
      request: { id: id1, body: "request 1" },
      response: "response 1",
    })
    await waitForNextUpdate()
    expect(hookInterface.current.getResult(id2)).toStrictEqual({
      request: { id: id2, body: "request 2" },
      response: "response 2",
    })
    await waitForNextUpdate()
    expect(hookInterface.current.getResult(id3)).toStrictEqual({
      request: { id: id3, body: "request 3" },
      response: "response 3",
    })
    expect(hookInterface.current.inProgress).toBe(false)
  })

  it("should provide the option to reset everything by canceling running requests and clearing results", async () => {
    const asyncFunction = asyncResponse({
      respondInMillis: 200,
      resolveWithMap: new Map([
        ["request 1", { response: "response 1" }],
        ["request 2", { response: "response 2" }],
        ["request 3", { response: "response 3" }],
      ]),
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useMultiAsync<string, string, string>({
        asyncFunction,
      })
    )

    let id1
    await act(async () => {
      id1 = hookInterface.current.startRequest("request 1")
    })
    await waitForMillis(10)

    await act(async () => {
      hookInterface.current.startRequest("request 2")
    })
    await waitForMillis(10)

    await act(async () => {
      hookInterface.current.startRequest("request 3")
    })
    await waitForNextUpdate()

    expect(hookInterface.current.getResult(id1)).toStrictEqual({
      request: { id: id1, body: "request 1" },
      response: "response 1",
    })
    act(() => {
      hookInterface.current.reset()
    })
    await waitForNextUpdate()
    expect(hookInterface.current.getResult(id1)).toBeUndefined()
    expect(hookInterface.current.results).toStrictEqual([])
    expect(hookInterface.current.inProgress).toBe(false)
  })

  it("should consider all requests, no matter their order", async () => {
    // eslint-disable-next-line no-undef
    const errorSpy = jest.spyOn(global.console, "error")
    const asyncFunction = asyncResponse({
      resolveWithMap: new Map([
        [
          "request 1",
          {
            response: "response 1",
            respondInMillis: 200,
          },
        ],
        [
          "request 2",
          {
            response: "response 2",
            respondInMillis: 50,
          },
        ],
        [
          "request 3",
          {
            response: "response 3",
            respondInMillis: 50,
          },
        ],
      ]),
    })
    const { result: hookInterface, waitForNextUpdate } = renderHook(() =>
      useMultiAsync<string, string, string>({
        asyncFunction,
      })
    )

    let id1
    await act(async () => {
      id1 = hookInterface.current.startRequest("request 1")
    })
    expect(hookInterface.current.inProgress).toBe(true)

    await waitForMillis(10)

    let id2
    await act(async () => {
      id2 = hookInterface.current.startRequest("request 2")
    })
    expect(hookInterface.current.inProgress).toBe(true)

    await waitForMillis(5)

    let id3
    await act(async () => {
      id3 = hookInterface.current.startRequest("request 3")
    })
    expect(hookInterface.current.inProgress).toBe(true)
    expect(hookInterface.current.getResult(id1)).toBeUndefined()
    expect(hookInterface.current.getResult(id2)).toBeUndefined()
    expect(hookInterface.current.getResult(id3)).toBeUndefined()

    await waitForNextUpdate()
    expect(hookInterface.current.inProgress).toBe(true)
    expect(hookInterface.current.getResult(id1)).toBeUndefined()
    expect(hookInterface.current.getResult(id2)).toStrictEqual({
      request: { id: id2, body: "request 2" },
      response: "response 2",
    })
    expect(hookInterface.current.getResult(id3)).toBeUndefined()

    await waitForNextUpdate()
    expect(hookInterface.current.inProgress).toBe(true)
    expect(hookInterface.current.getResult(id1)).toBeUndefined()
    expect(hookInterface.current.getResult(id2)).toStrictEqual({
      request: { id: id2, body: "request 2" },
      response: "response 2",
    })
    expect(hookInterface.current.getResult(id3)).toStrictEqual({
      request: { id: id3, body: "request 3" },
      response: "response 3",
    })

    await waitForNextUpdate()
    expect(hookInterface.current.getResult(id1)).toStrictEqual({
      request: { id: id1, body: "request 1" },
      response: "response 1",
    })
    expect(hookInterface.current.getResult(id2)).toStrictEqual({
      request: { id: id2, body: "request 2" },
      response: "response 2",
    })
    expect(hookInterface.current.getResult(id3)).toStrictEqual({
      request: { id: id3, body: "request 3" },
      response: "response 3",
    })
    expect(hookInterface.current.inProgress).toBe(false)
    // we would get console errors if act would occur outside of test
    expect(errorSpy).not.toHaveBeenCalled()
  })
})
