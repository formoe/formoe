// @flow
import type { Params, Result, Return } from "./index.js.flow"

import { useCallback, useEffect, useRef, useState } from "react"

function filterForUpdate<ArrayType>(
  array: ArrayType[],
  predicate: (entry: ArrayType) => boolean
): ArrayType[] {
  const filteredArray = array.filter((entry) => predicate(entry))
  // if nothing changes we need to hand back the original array to prevent unnecessary updates
  return filteredArray.length !== array.length ? filteredArray : array
}

export default function useMultiAsync<RequestType, ResultType, ErrorType>({
  asyncFunction,
  maxResults,
}: Params<RequestType, ResultType>): Return<
  RequestType,
  ResultType,
  ErrorType
> {
  const [requestsInProgress, setRequestsInProgress] = useState<symbol[]>([])
  const [returns, setReturns] = useState<
    Result<RequestType, ResultType, ErrorType>[]
  >([])
  const [results, setResults] = useState<
    Result<RequestType, ResultType, ErrorType>[]
  >([])

  // do not set any state after unmounting
  let canceled = useRef(false)
  useEffect(() => {
    canceled.current = false
    return () => {
      canceled.current = true
    }
  }, [])

  const startRequest = useCallback(
    (request) => {
      const doAsync = async (request) => {
        let asyncResponse
        try {
          setRequestsInProgress((currentInProgress) => {
            const newInProgress = [...currentInProgress, request.id]
            return newInProgress
          })
          asyncResponse = { response: await asyncFunction(request.body) }
        } catch (error) {
          asyncResponse = {
            error,
          }
        }
        if (!canceled.current) {
          setReturns((currentReturns) => [
            ...currentReturns,
            { request, ...asyncResponse },
          ])
        }
      }

      const id = Symbol()
      if (!canceled.current) {
        doAsync({ id, body: request })
      }
      return id
    },
    [asyncFunction]
  )

  useEffect(() => {
    if (!canceled.current && returns.length > 0) {
      setResults((currentResults) => {
        const newResults = [...currentResults]
        for (const returned of returns) {
          if (
            requestsInProgress.includes(returned.request.id) &&
            !newResults.find(
              (result) => result.request.id === returned.request.id
            )
          ) {
            newResults.push(returned)
          }
        }
        return newResults
      })
    }
  }, [returns, requestsInProgress])

  useEffect(() => {
    if (!canceled.current && results.length > 0) {
      const resultIds = results.map((result) => result.request.id)
      setRequestsInProgress((currentInProgress) =>
        filterForUpdate(currentInProgress, (id) => !resultIds.includes(id))
      )
      setReturns((currentReturns) =>
        filterForUpdate(
          currentReturns,
          (returned) => !resultIds.includes(returned.request.id)
        )
      )
    }
  }, [results])

  useEffect(() => {
    if (maxResults && results.length > maxResults) {
      setResults((currentResults) =>
        currentResults.slice(currentResults.length - maxResults)
      )
    }
  }, [results, maxResults])

  const getResult = useCallback(
    (id) =>
      results.find((result) => result.request && result.request.id === id),
    [results]
  )

  const clearResults = useCallback(() => setResults([]), [])

  const cancelAllRequests = useCallback(() => setRequestsInProgress([]), [])

  const cancelRequest = useCallback(
    (id) =>
      setRequestsInProgress((currentInProgress) =>
        filterForUpdate(currentInProgress, (reqId) => id !== reqId)
      ),
    []
  )

  const reset = useCallback(() => {
    cancelAllRequests()
    clearResults()
  }, [cancelAllRequests, clearResults])

  return {
    results,
    startRequest,
    getResult,
    inProgress: requestsInProgress.length > 0,
    clearResults,
    cancelAllRequests,
    cancelRequest,
    reset,
  }
}
