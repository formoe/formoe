declare module "@formoe/use-multi-async" {
  export type AsyncFunction<RequestType, ResponseType> = (
    request: RequestType
  ) => Promise<ResponseType>

  export type Params<RequestType, ResponseType> = {
    asyncFunction: AsyncFunction<RequestType, ResponseType>
    maxResults?: number
  }

  export type Request<RequestType> = {
    id: symbol
    body: RequestType
  }

  export type Result<RequestType, ResponseType, ErrorType> = {
    request: Request<RequestType>
    response?: ResponseType
    error?: ErrorType
  }

  export type Return<RequestType, ResponseType, ErrorType> = {
    results: Result<RequestType, ResponseType, ErrorType>[]
    startRequest: (request: RequestType) => symbol
    getResult: (
      id: symbol
    ) => Result<RequestType, ResponseType, ErrorType> | void
    inProgress: boolean
    clearResults: () => void
    cancelAllRequests: () => void
    cancelRequest: (id: symbol) => void
    reset: () => void
  }

  export default function useMultiAsync<RequestType, ResponseType, ErrorType>(
    params: Params<RequestType, ResponseType>
  ): Return<RequestType, ResponseType, ErrorType>
}
