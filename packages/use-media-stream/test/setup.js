/* eslint-disable no-undef */
/* getUserMedia mock */

global.userMedia = ({
  audio = true,
  video = true,
  audioBlocked = false,
  videoBlocked = false,
  noDevices = false,
} = {}) => {
  const devices = []
  if (audio) devices.push({ kind: "audioinput" })
  if (video) devices.push({ kind: "videoinput" })
  navigator.mediaDevices = noDevices
    ? undefined
    : {
        enumerateDevices: jest.fn(() => devices),
        getUserMedia: jest.fn().mockImplementation((config) => {
          if (config.audio && !audio) throw "No audio present"
          if (config.audio && audioBlocked) throw "Audio blocked"
          if (config.video && !video) throw "No video present"
          if (config.video && videoBlocked) throw "Video blocked"

          return {
            getTracks: jest.fn(() => [
              config.audio ? "AUDIO_TRACK" : "VIDEO_TRACK",
            ]),
          }
        }),
      }
}

global.MediaStream = jest.fn((tracks) => tracks)

jest.mock("../src/black-silence", () => ({
  black: jest.fn(() => "BLACK"),
  silence: jest.fn(() => "SILENCE"),
}))

// we also need to work around a react bug with the muted attribute
Object.defineProperty(HTMLMediaElement.prototype, "muted", {
  set: () => {},
})
