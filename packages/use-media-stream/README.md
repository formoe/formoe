# use-media-stream

The `useMediaStream` hook. The hook can be used to acquire `MediaStream` input objects from the users system using the `navigator.mediaDevices` interface.

## Usage

```
import { useMediaStream } from "@formoe/use-media-stream"

const streamConfig = {
  requestAudio: true,
  requestVideo: true,
  dummyAudio: true,
  dummyVideo: true,
}

const ownVideoPreview = useRef<null | HTMLMediaElement>(null)

const { streamResult } = useMediaStream({ streamConfig, display })

// initially mute outgoing audio tracks (mute own mic)
useEffect(() => {
  if (streamResult.response) {
    streamResult.response.getAudioTracks().forEach((track) => {
      track.enabled = false
    })
  }
}, [streamResult.response])

return <video ref={ownVideoPreview}></video>
```

### Returned Interface

The hook returns the `streamResult`. This is a [@formoe/use-async](../use-async/README.md) result. It contains the acquired `MediaStream` object as `response` if finished successfully and an error if the given configuration could not successfully be fullfilled.

### Params

The hook takes a configuration object containing two parameters:

#### Stream Configuration

`streamConfig` contains the configuration on which devices to acquire. 

The optional flags `requestAudio` and `requestVideo` mark whether or not the specified media type is optional:
* `undefined` means media will be aquired if possible but ignored if not
* `true` means media has to be aquired, resulting in an error response on failure
* `false` means meadia will not be aquired in any case

Acquiring media will prompt the user to allow access to the given media type.

The optional flags `dummyAudio` and `dummyVideo` mark whether or not a dummy track should be used if media is not aquired. For video this will result in a black 640x480px track and for audio in a silent track.
This is useful if you need media tracks (for example for a WebRTC connection) but can not aquire it from the user.

The `display` parameter is a `HTMLMediaElement`, that get's its `srcObject` set to the aquired stream on sucessful aquisition.

```JS
{
  streamConfig: {
    requestAudio?: boolean,
    requestVideo?: boolean,
    dummyAudio?: boolean,
    dummyVideo?: boolean,
  },
  display?: HTMLMediaElement | null,
}
```