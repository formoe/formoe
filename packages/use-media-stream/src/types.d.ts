import { Result } from "@formoe/use-async"

declare module "@formoe/use-media-stream" {
  interface StreamRequest {
    requestAudio?: boolean
    requestVideo?: boolean
    dummyAudio?: boolean
    dummyVideo?: boolean
    videoConstraints?: MediaTrackConstraints
  }

  interface Params {
    streamConfig: StreamRequest
    display?: HTMLMediaElement | null
  }

  interface Interface {
    streamResult: Result<StreamRequest, MediaStream, string>
  }

  export function useMediaStream(params: Params): Interface
}
