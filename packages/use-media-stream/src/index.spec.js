/* eslint-disable no-undef */
// @flow
import { waitFor } from "@testing-library/dom"
import { renderHook, act } from "@testing-library/react-hooks"

import "../test/setup"

import { useMediaStream } from "./index"

describe("useMediaStream", () => {
  describe("if all media devices are accessible", () => {
    beforeAll(() => {
      global.userMedia()
    })
    it("should retrieve audio and video tracks if everything works out", async () => {
      const streamConfig = {}
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.response).toEqual(["VIDEO_TRACK", "AUDIO_TRACK"])
        })
      })
    })

    describe("if video is prohibitted", () => {
      it("should retrieve only audio tracks if dummy video is not allowed", async () => {
        const streamConfig = { requestVideo: false }
        const { result: hookInterface } = renderHook(() =>
          useMediaStream({ streamConfig })
        )

        await act(async () => {
          await waitFor(() => {
            const { streamResult } = hookInterface.current
            expect(streamResult.response).toStrictEqual(["AUDIO_TRACK"])
          })
        })
      })

      it("should retrieve audio tracks and dummy video if video is requested as dummy", async () => {
        const streamConfig = { requestVideo: false, dummyVideo: true }
        const { result: hookInterface } = renderHook(() =>
          useMediaStream({ streamConfig })
        )

        await act(async () => {
          await waitFor(() => {
            const { streamResult } = hookInterface.current
            expect(streamResult.response).toStrictEqual([
              "BLACK",
              "AUDIO_TRACK",
            ])
          })
        })
      })
    })

    describe("if audio is prohibitted", () => {
      it("should retrieve only video tracks if dummy audio is not allowed", async () => {
        const streamConfig = { requestAudio: false }
        const { result: hookInterface } = renderHook(() =>
          useMediaStream({ streamConfig })
        )

        await act(async () => {
          await waitFor(() => {
            const { streamResult } = hookInterface.current
            expect(streamResult.response).toStrictEqual(["VIDEO_TRACK"])
          })
        })
      })

      it("should retrieve video tracks and dummy audio if audio is requested as dummy", async () => {
        const streamConfig = { requestAudio: false, dummyAudio: true }
        const { result: hookInterface } = renderHook(() =>
          useMediaStream({ streamConfig })
        )

        await act(async () => {
          await waitFor(() => {
            const { streamResult } = hookInterface.current
            expect(streamResult.response).toStrictEqual([
              "VIDEO_TRACK",
              "SILENCE",
            ])
          })
        })
      })
    })

    it("should retrieve nothing if audio and video are prohibitted and no dummies are allowed (but should still work)", async () => {
      const streamConfig = { requestAudio: false, requestVideo: false }
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.response).toStrictEqual([])
        })
      })
    })
  })

  describe("if no video device exists", () => {
    beforeAll(() => {
      global.userMedia({ video: false })
    })

    it("should retrieve only audio tracks if dummy video is not allowed", async () => {
      const streamConfig = {}
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.response).toStrictEqual(["AUDIO_TRACK"])
        })
      })
    })

    it("should retrieve audio tracks and dummy video if allowed", async () => {
      const streamConfig = { dummyVideo: true }
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.response).toStrictEqual(["BLACK", "AUDIO_TRACK"])
        })
      })
    })

    it("should yield an error if video is explicitly requested", async () => {
      const streamConfig = { requestVideo: true }
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.error).toBe(
            "Mandatory device of kind videoinput not found"
          )
        })
      })
    })
  })

  describe("if video device is blocked", () => {
    beforeAll(() => {
      global.userMedia({ videoBlocked: true })
    })

    it("should retrieve only audio tracks if dummy video is not allowed", async () => {
      const streamConfig = {}
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.response).toStrictEqual(["AUDIO_TRACK"])
        })
      })
    })

    it("should retrieve audio tracks and dummy video if allowed", async () => {
      const streamConfig = { dummyVideo: true }
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.response).toStrictEqual(["BLACK", "AUDIO_TRACK"])
        })
      })
    })

    it("should yield an error if video is explicitly requested", async () => {
      const streamConfig = { requestVideo: true }
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.error).toBe("Video blocked")
        })
      })
    })
  })

  describe("if no audio device exists", () => {
    beforeAll(() => {
      global.userMedia({ audio: false })
    })

    it("should retrieve only video tracks if dummy audi is not allowed", async () => {
      const streamConfig = {}
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.response).toStrictEqual(["VIDEO_TRACK"])
        })
      })
    })

    it("should retrieve video tracks and dummy audio if allowed", async () => {
      const streamConfig = { dummyAudio: true }
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.response).toStrictEqual([
            "VIDEO_TRACK",
            "SILENCE",
          ])
        })
      })
    })

    it("should yield an error if audio is explicitly requested", async () => {
      const streamConfig = { requestAudio: true }
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.error).toBe(
            "Mandatory device of kind audioinput not found"
          )
        })
      })
    })
  })

  describe("if audio device is blocked", () => {
    beforeAll(() => {
      global.userMedia({ audioBlocked: true })
    })

    it("should retrieve only video tracks if dummy audi is not allowed", async () => {
      const streamConfig = {}
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.response).toStrictEqual(["VIDEO_TRACK"])
        })
      })
    })

    it("should retrieve video tracks and dummy audio if allowed", async () => {
      const streamConfig = { dummyAudio: true }
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.response).toStrictEqual([
            "VIDEO_TRACK",
            "SILENCE",
          ])
        })
      })
    })

    it("should yield an error if audio is explicitly requested", async () => {
      const streamConfig = { requestAudio: true }
      const { result: hookInterface } = renderHook(() =>
        useMediaStream({ streamConfig })
      )

      await act(async () => {
        await waitFor(() => {
          const { streamResult } = hookInterface.current
          expect(streamResult.error).toBe("Audio blocked")
        })
      })
    })
  })

  it("should yield an error if no devices are present", async () => {
    global.userMedia({ noDevices: true })
    const streamConfig = {}
    const { result: hookInterface } = renderHook(() =>
      useMediaStream({ streamConfig })
    )

    await act(async () => {
      await waitFor(() => {
        const { streamResult } = hookInterface.current
        expect(streamResult.error).toBe("No media devices accessible")
      })
    })
  })

  it("should set the displays srcObject on successful response", async () => {
    global.userMedia()
    const streamConfig = {}
    const display = {}
    const { result: hookInterface } = renderHook(() =>
      // $FlowFixMe circumvent difficult mock
      useMediaStream({ streamConfig, display })
    )

    await act(async () => {
      await waitFor(() => {
        const { streamResult } = hookInterface.current
        expect(streamResult.inProgress).toBe(false)
      })
    })
    expect(display.srcObject).toStrictEqual(["VIDEO_TRACK", "AUDIO_TRACK"])
  })
})
