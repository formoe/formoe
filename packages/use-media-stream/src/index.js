//@flow
import { useEffect } from "react"
import useAsync from "@formoe/use-async"
import { black, silence } from "./black-silence"

import type { Params, Interface, StreamRequest } from "./index.js.flow"

/**
 * @returns the acquired stream or nothing if the stream could not be aqcuired for any reasons but is not explicitly requested.
 * @throws if an explicitly requested stream could not be acquired.
 * @param {string} kind the type of media device requested. So far supported are "audioinput" or "videoinput"
 * @param {*} mediaDevices the mediadevices interface of the system
 * @param {*} devices the devices present in the system
 * @param {boolean} requested indicating if a devicetype is explicitly needed (true), shall explicitly not be acquired (false)
 * or is optional (undefined).
 * If optional devices are acquired if possible, but ignored if not (not present / blocked by user etc.)
 */
const acquireStream = async (
  kind,
  mediaDevices,
  devices,
  requested,
  videoConstraints
) => {
  // don't try to acquire anything if explicitly forbidden
  if (requested === false) return

  // No devices of the given kind found...
  if (!devices.find((device) => device.kind === kind)) {
    // ... if explicitly requested, we can't use dummy tracks and throw...
    if (requested) throw `Mandatory device of kind ${kind} not found`

    // ... if not we just return, there may be dummy tracks injected later
    // or we don't really care if the media could be aquired or not and ignore it
    return
  }

  try {
    return await mediaDevices.getUserMedia(
      kind === "audioinput"
        ? { audio: true }
        : { video: videoConstraints || true }
    )
  } catch (err) {
    // if explicitly requested, we can't use dummy tracks and throw...
    if (requested) throw err
    // otherwise we ignore the error and just return, there may be dummy tracks injected later
    // or we don't really care if the media could be aquired or not and ignore it
  }
}

const findTrack = (stream, useDummy, createDummy) => {
  // Can we acquire the requested tracks?
  if (!stream) {
    if (!useDummy) return

    // Otherwise we fill up anything with dummys that we don't get from the user
    return createDummy()
  }

  return stream.getTracks()[0]
}

const requestStream = async ({
  requestAudio,
  requestVideo,
  dummyAudio,
  dummyVideo,
  videoConstraints,
}: StreamRequest) => {
  const mediaDevices = navigator.mediaDevices
  if (!mediaDevices) {
    throw "No media devices accessible"
  }

  const devices = await mediaDevices.enumerateDevices()

  let videoTrack = findTrack(
    await acquireStream(
      "videoinput",
      mediaDevices,
      devices,
      requestVideo,
      videoConstraints
    ),
    dummyVideo,
    black
  )
  let audioTrack = findTrack(
    await acquireStream(
      "audioinput",
      mediaDevices,
      devices,
      requestAudio,
      videoConstraints
    ),
    dummyAudio,
    silence
  )
  const tracks = []

  if (videoTrack) tracks.push(videoTrack)
  if (audioTrack) tracks.push(audioTrack)

  // $FlowFixMe typings are not up to date, the constructor is documented here: https://developer.mozilla.org/en-US/docs/Web/API/MediaStream/MediaStream
  return new MediaStream(tracks)
}

export const useMediaStream = ({
  streamConfig,
  display,
}: Params): Interface => {
  const { result: streamResult, setRequest: requestMediaStream } = useAsync({
    asyncFunction: requestStream,
  })

  useEffect(() => {
    requestMediaStream(streamConfig)
  }, [streamConfig, requestMediaStream])

  useEffect(() => {
    if (!display || !streamResult.response) return

    display.srcObject = streamResult.response

    return () => {
      display.srcObject = undefined
    }
  }, [streamResult, display])

  return {
    streamResult,
  }
}
