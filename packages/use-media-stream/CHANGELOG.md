# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.4.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-media-stream@0.3.0...@formoe/use-media-stream@0.4.0) (2023-12-05)


### Features

* **use-media-stream:** publish commit ([36a5f93](https://gitlab.com/formoe/formoe/commit/36a5f93a49f5f707f6721fc0c8f2fe3264910920))





# [0.3.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-media-stream@0.2.0...@formoe/use-media-stream@0.3.0) (2023-12-05)


### Features

* **use-media-stream:** stream with video constrains ([fcd83a6](https://gitlab.com/formoe/formoe/commit/fcd83a6236226db97f24460d1fda860ea9afed5a))





# [0.2.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-media-stream@0.1.6...@formoe/use-media-stream@0.2.0) (2022-10-07)


### Features

* **use-media-stream:** add TypeScript types ([c73cfb7](https://gitlab.com/formoe/formoe/commit/c73cfb779cb988cec57127ea7c0e512fefc0e24a))





## [0.1.6](https://gitlab.com/formoe/formoe/compare/@formoe/use-media-stream@0.1.5...@formoe/use-media-stream@0.1.6) (2022-09-21)

**Note:** Version bump only for package @formoe/use-media-stream





## [0.1.5](https://gitlab.com/formoe/formoe/compare/@formoe/use-media-stream@0.1.4...@formoe/use-media-stream@0.1.5) (2022-09-19)

**Note:** Version bump only for package @formoe/use-media-stream





## [0.1.4](https://gitlab.com/formoe/formoe/compare/@formoe/use-media-stream@0.1.3...@formoe/use-media-stream@0.1.4) (2022-09-19)

**Note:** Version bump only for package @formoe/use-media-stream





## [0.1.3](https://gitlab.com/formoe/formoe/compare/@formoe/use-media-stream@0.1.2...@formoe/use-media-stream@0.1.3) (2022-09-13)

**Note:** Version bump only for package @formoe/use-media-stream





## [0.1.2](https://gitlab.com/formoe/formoe/compare/@formoe/use-media-stream@0.1.1...@formoe/use-media-stream@0.1.2) (2021-05-20)

**Note:** Version bump only for package @formoe/use-media-stream





## [0.1.1](https://gitlab.com/formoe/formoe/compare/@formoe/use-media-stream@0.1.0...@formoe/use-media-stream@0.1.1) (2021-05-19)


### Bug Fixes

* **use-media-stream:** fix main in package json ([b33950d](https://gitlab.com/formoe/formoe/commit/b33950d420287c4e8771e8c40dd6ba4d04b43623))





# 0.1.0 (2021-05-18)


### Features

* **use-media-stream:** introduce hook to acquire meda streams ([3410561](https://gitlab.com/formoe/formoe/commit/34105612d6b42e39b384a63a83bf1adaaaa4a274))
