# use-webrtc

The `useWebRTC` hook provides functionality to negotiate and establish WebRTC connections. It implements the [perfect negotiation](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Perfect_negotiation) with a polite and implolite peer.

# Usage

```JavaScript
import { useWebRTC } from "@formoe/use-webrtc"

const connectionConfig = undefined

const onAnswer = (sdp) => {
    // signal to other peer
}

const onOffer = (sdp) => {
    // signal to other peer
}

const onIceCandidate = (candidate) => {
    // signal to other peer
}

const MyWebRTCComponent = ({stream}) => {

    const videoRef = useRef<null | HTMLMediaElement>(null)

    const {
        acceptIncomingOffer,
        acceptIncomingAnswer,
        addIceCandidate,
        rtcPeerConnection,
    } = useWebRTC({
        connectionConfig,
        active: true,
        polite: false,
        view: videoRef.current,
        localStream: stream,
        onOffer,
        onAnswer,
        onIceCandidate,
        initiateOffer: true,
    })

    return <video
            width="100%"
            height="100%"
            ref={videoRef}
            autoPlay
            playsInline
            controls
          >
            Your browser does not support the video tag.
          </video>
}
```

## Params

The hook can be configured via the following params:

* `connectionConfig`: A [RTCConfiguration](https://developer.mozilla.org/en-US/docs/Web/API/RTCConfiguration) object to initialise the connection with.
* `active`: A `boolean` deciding if the connection shall be established. If set to false any existing connection will be closed. If set to true a connection will be created and negotiation commences as needed.
* `polite`: A `boolean` indicating whether this is the polite or impolite client. You always need to have one polite and one impolite client talking to each other. So this should be a static config for each client.
* `view`: A [HTMLMediaElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement) to show the incoming (the peers) stream.
* `localStream`: A [MediaStream](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream) object containing the stream to send to the peer. This has to be set WebRTC will start negotiating. Be aware, that you may need tracks for each media kind (video and audio) you want to be transported on the offer creating side. So for example if you only send audio tracks from the offer side but would want to send video from the answering side, you need to warm up with a dummy track. You can consider using [@formoe/use-media-stream](../packages/use-media-stream/README.md) to acquire streams from the user.
* `onOffer`: Function to call if the connection generates an offer. Use this to send the sdp through your signaling channel to the other peer.
* `onAnswer`: Function to call if the connection generates an answer. Use this to send the sdp through your signaling channel to the other peer.
* `onIceCandidate`: Function to call if the connection generates an [RTCIceCandidate](https://developer.mozilla.org/de/docs/Web/API/RTCIceCandidate). Use this to send the candidate through your signaling channel to the other peer.
* `initiateOffer`: At the moment this indicates if we are the side initiating an offer (making the call). This will soon be handled by the polite / impolite connection logic to allow bidirectional calls.

## Returned Interface

* `acceptIncomingOffer`: A function taking an incoming sdp string to set as offer for the connection.
* `acceptIncomingAnswer`: A function taking an incoming sdp string to set as answer for the connection.
* `addIceCandidate`: A function taking an incoming RTCIceCandidate to add as candidate for the connection.
* `rtcPeerConnection`: The [RTCPeerConnection](https://developer.mozilla.org/de/docs/Web/API/RTCPeerConnection) the hook currently handles.
