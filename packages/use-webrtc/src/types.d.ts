declare module "@formoe/use-webrtc" {
  export type Params = {
    active?: boolean
    initiateOffer?: boolean
    polite: boolean
    connectionConfig?: RTCConfiguration
    onOffer: (sdp: string) => void
    onAnswer: (sdp: string) => void
    onIceCandidate: (candidate: RTCIceCandidate) => void
    view?: HTMLMediaElement | null
    localStream: MediaStream
    onRemoteStream?: (remoteStream: MediaStream) => void
  }

  export type Interface = {
    rtcPeerConnection?: RTCPeerConnection
    rtcDataChannel?: RTCDataChannel
    acceptIncomingAnswer: (sdp: string) => void
    acceptIncomingOffer: (sdp: string) => void
    addIceCandidate: (candidate: RTCIceCandidate) => symbol | undefined
  }

  export default function useWebRTC(params: Params): Interface
}
