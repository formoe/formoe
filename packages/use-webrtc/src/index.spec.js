// @flow
import { renderHook, act } from "@testing-library/react-hooks"
import {
  initMockConnection,
  mockConnection,
  mockLocalSDP,
} from "../test/test-utils"
import useWebRTC from "./index"

const onOffer = jest.fn()
const onAnswer = jest.fn()
const onIceCandidate = jest.fn()

const localStream = {
  getTracks: jest.fn(() => ["TRACK_1", "TRACK_2"]),
}

beforeEach(() => {
  onOffer.mockClear()
  onAnswer.mockClear()
  onIceCandidate.mockClear()
  initMockConnection()
  jest.useFakeTimers()
})

afterEach(() => {
  jest.runOnlyPendingTimers()
  jest.useRealTimers()
})

type HookProps = {
  polite: boolean,
  active?: boolean,
  connectionConfig?: RTCConfiguration,
  view?: {},
}

const renderWebRTCHook = ({
  polite,
  active,
  connectionConfig,
  view,
}: HookProps) => {
  const {
    result: hookInterface,
    rerender,
    unmount,
    waitForNextUpdate,
  } = renderHook(
    ({ active }) =>
      useWebRTC({
        connectionConfig,
        polite,
        active,
        onOffer,
        onAnswer,
        onIceCandidate,
        // $FlowFixMe mock stream
        localStream,
        // $FlowFixMe mock HTMLMediaElement
        view,
        initiateOffer: true,
      }),
    {
      initialProps: {
        active,
      },
    }
  )
  const setProps = ({ active }: { active: boolean }) => {
    rerender({
      active,
    })
  }
  return { hookInterface, setProps, unmount, waitForNextUpdate }
}

describe("useWebRTC", () => {
  describe.each`
    polite
    ${true}
    ${false}
  `("if polite is set to $polite", ({ polite }) => {
    it("should do nothing if not set to active", () => {
      const { hookInterface } = renderWebRTCHook({ polite })
      const { rtcPeerConnection } = hookInterface.current
      expect(rtcPeerConnection).toBeUndefined()
      expect(global.RTCPeerConnection).not.toHaveBeenCalled()
    })

    it.each`
      connectionConfig
      ${undefined}
      ${{ someProp: "someValue to test config is used" }}
    `(
      "should set the connection with the given config: $connectionConfig when active and connect the stream and start negotiation",
      ({ connectionConfig }) => {
        const { hookInterface } = renderWebRTCHook({
          polite,
          active: true,
          connectionConfig,
        })

        const { rtcPeerConnection } = hookInterface.current
        expect(rtcPeerConnection).toBe(mockConnection)
        expect(rtcPeerConnection.connectionConfig).toBe(connectionConfig)
        expect(rtcPeerConnection.addTrack).toHaveBeenCalledWith(
          "TRACK_1",
          localStream
        )
        expect(rtcPeerConnection.addTrack).toHaveBeenCalledWith(
          "TRACK_2",
          localStream
        )
      }
    )

    describe("when initiating the call", () => {
      it("should create an offer if negotiation is started", async () => {
        const { hookInterface, waitForNextUpdate } = renderWebRTCHook({
          polite,
          active: true,
        })
        const { rtcPeerConnection } = hookInterface.current
        act(() => {
          rtcPeerConnection.onnegotiationneeded()
        })
        await waitForNextUpdate()
        // on negotiation the connection should create an offer via calling local description...
        expect(rtcPeerConnection.setLocalDescription).toHaveBeenCalled()
        // ... and send it to the conusmers onOffer callback
        expect(onOffer).toHaveBeenCalledWith(mockLocalSDP)
      })

      it("should accept an incoming answer", async () => {
        const { hookInterface, waitForNextUpdate } = renderWebRTCHook({
          polite,
          active: true,
        })
        const {
          rtcPeerConnection,
          acceptIncomingAnswer,
        } = hookInterface.current
        act(() => {
          acceptIncomingAnswer("INCOMING ANSWER")
        })
        await waitForNextUpdate()

        // on recieving an answer the remote description should be set
        expect(rtcPeerConnection.setRemoteDescription).toHaveBeenCalledWith({
          sdp: "INCOMING ANSWER",
          type: "answer",
        })
      })
    })

    describe("when recieving a track", () => {
      const simulateOnTrack = (rtcPeerConnection) => {
        const track = {}
        const streams = ["STREAM"]
        act(() => {
          rtcPeerConnection.ontrack({ track, streams })
          track.onunmute()
        })
      }

      it("should cope with no view set", () => {
        const { hookInterface } = renderWebRTCHook({
          polite,
          active: true,
        })
        const { rtcPeerConnection } = hookInterface.current
        simulateOnTrack(rtcPeerConnection)
      })

      it("should connect it to the given view", () => {
        const view = {}
        const { hookInterface } = renderWebRTCHook({
          polite,
          active: true,
          view,
        })
        const { rtcPeerConnection } = hookInterface.current
        simulateOnTrack(rtcPeerConnection)
        expect(view.srcObject).toBe("STREAM")
      })
    })

    describe("when recieving a call", () => {
      it("should connect the offer and create an answer", async () => {
        const { hookInterface, waitForNextUpdate } = renderWebRTCHook({
          polite,
          active: true,
        })
        const { rtcPeerConnection, acceptIncomingOffer } = hookInterface.current
        act(() => {
          acceptIncomingOffer("INCOMING OFFER")
        })
        await waitForNextUpdate()

        // on recieving an offer the remote description should be set...
        expect(rtcPeerConnection.setRemoteDescription).toHaveBeenCalledWith({
          sdp: "INCOMING OFFER",
          type: "offer",
        })
        // ... an answer created as local description...
        expect(rtcPeerConnection.setLocalDescription).toHaveBeenCalled()
        // ... and sent to the conusmers onAnswer callback
        expect(onAnswer).toHaveBeenCalledWith(mockLocalSDP)
      })
    })

    it("should announce created candidates", async () => {
      const { hookInterface } = renderWebRTCHook({
        polite,
        active: true,
      })
      const { rtcPeerConnection } = hookInterface.current
      act(() => {
        rtcPeerConnection.onicecandidate({ candidate: "CANDIDATE" })
      })

      expect(onIceCandidate).toHaveBeenCalledWith("CANDIDATE")

      act(() => {
        rtcPeerConnection.onicecandidate({ candidate: "ANOTHER CANDIDATE" })
      })

      expect(onIceCandidate).toHaveBeenCalledWith("ANOTHER CANDIDATE")
    })

    it("should set incoming candidates", async () => {
      const { hookInterface, waitForNextUpdate } = renderWebRTCHook({
        polite,
        active: true,
      })
      const { rtcPeerConnection, addIceCandidate } = hookInterface.current
      act(() => {
        addIceCandidate("CANDIDATE 1")
        addIceCandidate("CANDIDATE 2")
      })
      await waitForNextUpdate()

      expect(rtcPeerConnection.addIceCandidate).toHaveBeenCalledWith(
        "CANDIDATE 1"
      )
      expect(rtcPeerConnection.addIceCandidate).toHaveBeenCalledWith(
        "CANDIDATE 2"
      )
    })

    it("should close the connection on setting inactive", () => {
      const { setProps, hookInterface } = renderWebRTCHook({
        polite,
        active: true,
      })
      const { rtcPeerConnection } = hookInterface.current
      setProps({
        active: false,
      })
      expect(rtcPeerConnection.close).toHaveBeenCalled()
    })

    it("should close the connection on unmount", () => {
      const { unmount, hookInterface } = renderWebRTCHook({
        polite,
        active: true,
      })
      const { rtcPeerConnection } = hookInterface.current
      unmount()
      expect(rtcPeerConnection.close).toHaveBeenCalled()
    })
  })
})
