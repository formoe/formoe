// @flow
import { useCallback, useEffect, useState } from "react"
import useMultiAsync from "@formoe/use-multi-async"
import useAsync from "@formoe/use-async"

import type { Params, Interface } from "./index.js.flow"

type CreateOfferRequest = {|
  connection: RTCPeerConnection,
|}

type SDPRequest = {|
  connection: RTCPeerConnection,
  sdp: string,
|}

type AddCandidateRequest = {|
  connection: RTCPeerConnection,
  candidate?: RTCIceCandidate,
  ignoreOffer: boolean,
|}

const connectStream = (stream: MediaStream, connection: RTCPeerConnection) => {
  for (const track of stream.getTracks()) {
    connection.addTrack(track, stream)
  }
}

const addCandidate = async ({ connection, candidate, ignoreOffer }) => {
  try {
    await connection.addIceCandidate(candidate)
  } catch (err) {
    // Suppress ignored offer's candidates
    if (!ignoreOffer) {
      console.error(
        "addCandidate - error while adding WebRTC ice candidate:",
        err
      )
      throw err
    }
  }
}

const createLocalDesription = async ({ connection }) => {
  await connection.setLocalDescription()
  return connection.localDescription && connection.localDescription.sdp
}

const connectAnswer = async ({ connection, sdp }) => {
  await connection.setRemoteDescription({ sdp, type: "answer" })
}

const connectOffer = async ({ connection, sdp }) => {
  await connection.setRemoteDescription({ sdp, type: "offer" })
  // this is not completely clear, I suppose WebRTC somehow handles this internally
  // and decides if it's better to create an own offer or set the given one as SRD rolls back as needed.
  return await createLocalDesription({ connection })
}

export default function useWebRTC({
  connectionConfig,
  polite,
  active = false,
  view,
  localStream,
  onRemoteStream,
  onOffer,
  onAnswer,
  onIceCandidate,
  initiateOffer = false,
}: Params): Interface {
  const [connection, setConnection] = useState<RTCPeerConnection | void>()
  const [stream, setStream] = useState()
  const [dataChannel, setDataChannel] = useState()

  const {
    result: createOfferResult,
    setRequest: requestCreateOffer,
  } = useAsync<CreateOfferRequest, ?string, Error>({
    asyncFunction: createLocalDesription,
  })

  useEffect(() => {
    if (active) {
      const newConnection = new RTCPeerConnection(connectionConfig)
      newConnection.ontrack = ({ track, streams }) => {
        // once media for a remote track arrives, show it in the view element
        track.onunmute = () => {
          onRemoteStream && onRemoteStream(streams[0])
          setStream(streams[0])
        }
      }
      // offer side
      // let the "negotiationneeded" event trigger offer generation
      newConnection.onnegotiationneeded = () => {
        // TODO: this should have been handled by negotiation logic and needs fixing as soon as we want bidirectional calls
        if (!initiateOffer) return

        let _dataChannel = newConnection.createDataChannel("annotation-channel")

        _dataChannel.onopen = () => {
          setDataChannel(_dataChannel)
        }

        requestCreateOffer({ connection: newConnection })
      }
      // candidates going out
      // the onIceCandidate should never change for one instance of the hook
      newConnection.onicecandidate = ({ candidate }) => {
        if (candidate) {
          onIceCandidate(candidate)
        }
      }

      setConnection(newConnection)
    } else {
      setConnection((connection) => {
        if (connection) {
          connection.close()
        }
      })
    }
  }, [
    connectionConfig,
    active,
    requestCreateOffer,
    onIceCandidate,
    initiateOffer,
    onRemoteStream,
  ])

  useEffect(() => {
    return () => {
      if (connection) {
        connection.close()
      }
    }
  }, [connection])

  /**
   * Offer - answer negotiation
   */

  // --- The perfect negotiation logic ---

  // outgoing offer
  useEffect(() => {
    if (!createOfferResult.response || !onOffer) return

    onOffer(createOfferResult.response)
  }, [createOfferResult, onOffer])

  // => leads to => incoming answer eventually
  const {
    result: connectAnswerResult,
    setRequest: requestConnectAnswer,
  } = useAsync<SDPRequest, void, Error>({
    asyncFunction: connectAnswer,
  })

  const acceptIncomingAnswer = useCallback(
    (sdp) => {
      if (!connection || !sdp) return

      requestConnectAnswer({ connection, sdp })
    },
    [connection, requestConnectAnswer]
  )

  // incoming offer
  const readyForOffer =
    connection &&
    !createOfferResult.inProgress &&
    (connection.signalingState == "stable" || connectAnswerResult.inProgress)
  const ignoreOffer = !polite && !readyForOffer

  const {
    result: connectOfferResult,
    setRequest: requestConnectOffer,
  } = useAsync<SDPRequest, ?string, Error>({
    asyncFunction: connectOffer,
  })

  const acceptIncomingOffer = useCallback(
    (sdp) => {
      if (!connection || !sdp) return

      if (ignoreOffer) return

      // listen for a data channel on the peer connection
      connection.ondatachannel = (event) => {
        let _dataChannel = event.channel

        // listen for the open event on the data channel
        _dataChannel.onopen = () => {
          setDataChannel(_dataChannel)
        }
      }

      requestConnectOffer({ connection, sdp })
    },
    [connection, requestConnectOffer, ignoreOffer]
  )

  // => leads to => created outgoing answer
  useEffect(() => {
    if (!connectOfferResult.response || !onAnswer) return

    onAnswer(connectOfferResult.response)
  }, [connectOfferResult.response, onAnswer])

  /**
   * Candidate handling
   */

  // incoming candidates
  const {
    startRequest: requestAddCandidate,
    reset: resetCandidates,
  } = useMultiAsync<AddCandidateRequest, void, Error>({
    asyncFunction: addCandidate,
    maxResults: 20,
  })

  const addIceCandidate = useCallback(
    (candidate) => {
      if (!connection) {
        return
      }
      return requestAddCandidate({
        connection,
        candidate,
        ignoreOffer,
      })
    },
    [connection, requestAddCandidate, ignoreOffer]
  )

  // on connection change candidates waiting for processing are thrown away
  useEffect(() => {
    return () => {
      resetCandidates()
    }
  }, [connection, resetCandidates])

  /**
   * Stream handling
   * - connect incoming stream to view element
   */

  useEffect(() => {
    if (!connection) return

    connectStream(localStream, connection)
  }, [connection, localStream])

  useEffect(() => {
    // don't set srcObject again if it is already set.
    if (!connection || !view || view.srcObject) return

    view.srcObject = stream

    return () => {
      view.srcObject = undefined
    }
  }, [view, connection, stream])

  /**
   * (error) logging
   */

  useEffect(() => {
    if (connectAnswerResult.error) {
      console.error(
        "error while connecting WebRTC answer:",
        connectAnswerResult.error
      )
    }
  }, [connectAnswerResult.error])

  useEffect(() => {
    if (createOfferResult.error) {
      console.error(
        "error while creating WebRTC offer:",
        createOfferResult.error
      )
    }
  }, [createOfferResult.error])

  useEffect(() => {
    if (connectOfferResult.error) {
      console.error(
        "error while connecting WebRTC offer and creating answer:",
        connectOfferResult.error
      )
    }
  }, [connectOfferResult.error])

  return {
    rtcPeerConnection: connection,
    rtcDataChannel: dataChannel,
    acceptIncomingAnswer,
    acceptIncomingOffer,
    addIceCandidate,
  }
}
