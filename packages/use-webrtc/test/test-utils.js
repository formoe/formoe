/* eslint-disable no-undef */

export let mockConnection
export const mockLocalSDP = "localSDP"

const mockConnectionFactory = (connectionConfig) => {
  const connection = {
    close: jest.fn(),
    localDescription: { sdp: mockLocalSDP },
    connectionConfig,
    signalingState: "stable",
  }
  connection.setLocalDescription = jest.fn()
  connection.setRemoteDescription = jest.fn((rD) => {
    connection.remoteDescription = rD
  })
  // as soon as we have all descriptions and some candidate, set to stable
  connection.addIceCandidate = jest.fn()
  connection.addTrack = jest.fn()

  const setDataChannel = jest.fn() // mock function
  const _dataChannel = {
    onopen: () => setDataChannel(_dataChannel),
  }

  connection.createDataChannel = jest.fn(() => _dataChannel)

  // call the function that creates the data channel
  connection.createDataChannel("annotation-channel")

  return connection
}

export const initMockConnection = () => {
  global.RTCIceCandidate = jest.fn()

  global.RTCPeerConnection = jest.fn((connectionConfig) => {
    mockConnection = mockConnectionFactory(connectionConfig)
    return mockConnection
  })
}
