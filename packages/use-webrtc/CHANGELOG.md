# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.2.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-webrtc@1.1.0...@formoe/use-webrtc@1.2.0) (2023-03-20)


### Features

* **use-webrtc:** add types ([900ae8e](https://gitlab.com/formoe/formoe/commit/900ae8ebdcfb84207bf5c3e28ac05fb1e9f82caf))





# [1.1.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-webrtc@1.0.0...@formoe/use-webrtc@1.1.0) (2022-03-29)


### Features

* **use-webrtc:** add event for stream acquisition ([b7b771f](https://gitlab.com/formoe/formoe/commit/b7b771fee637fc183df198d67fecbb827b1f6639))





# [1.0.0](https://gitlab.com/formoe/formoe/compare/@formoe/use-webrtc@1.0.1...@formoe/use-webrtc@1.0.0) (2021-06-28)


### Continuous Integration

* **use-webrtc:** prepare component publish ([8032d07](https://gitlab.com/formoe/formoe/commit/8032d077987ff7d3f3b3ecf5bdba15c8ebb9083d))


### BREAKING CHANGES

* **use-webrtc:** First publish





## 1.0.1 (2021-05-27)

**Note:** Version bump only for package @formoe/use-webrtc
