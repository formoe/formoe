/* eslint-disable */
module.exports = function (api) {
  const apiEnv = api.env()
  const presetEnvConfig = {
    useBuiltIns: "usage",
    bugfixes: true,
    corejs: { version: 3 },
  }

  // compile for node in tests
  if (apiEnv === "test") {
    presetEnvConfig.targets = { node: "current" }
  }

  const config = {
    presets: [
      ["@babel/preset-env", presetEnvConfig],
      "@babel/preset-react",
      "@babel/preset-flow",
    ],
    babelrcRoots: [".", "packages/*"],
    comments: false,
  }

  // don't compile tests for prod builds
  if (apiEnv === "production") {
    config.ignore = ["packages/**/*.spec.js", "packages/**/*.test.js"]
  }

  return config
}
