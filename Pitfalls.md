# Some common pitfalls you may stumble upon while developing

## Local Dependencies

They might lead to confusion due to the symlinking behaviour of `lerna`. [Read this](CheatSheet.md#local-dependencies) on how to deal with them.
