import React from "react"
import ComponentPlaceholder from "../src/index"

export default {
  title: "Path/To",
  component: ComponentPlaceholder,
  argTypes: {
    // someChoiceProp: {
    //   control: { type: "select", options: ["submit", "button", "reset"] },
    // },
    // onSomeAction: {
    //   action: "someAction occured",
    //   table: {
    //     disable: true,
    //   },
    // },
  },
}

export const ComponentPlaceholderDefault = (args) => (
  <ComponentPlaceholder {...args} />
)
ComponentPlaceholder.args = {
  someProp: "someProp",
}
