// @flow
import * as React from "react"
import classNames from "classnames"

import type { Props } from "./index.js.flow"

import "./component.scss"

export default function ComponentPlaceholder({ someProp }: Props): React.Node {
  return <div classNames={classNames("package-placeholder")}>{someProp}</div>
}
