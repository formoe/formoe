// @flow
import type { Params, Return } from "./index.js.flow"

export default function foo({ param = true }: Params): Return {
  return param ? { prop: "true" } : { prop: "false" }
}
