// @flow
import { useState } from "react"
import type { Params, Interface } from "./index.js.flow"

export default (params: Params): Interface => {
  const { param = false } = params || {}
  const [interfaceProp, setInterfaceProp] = useState(param ? "true" : "false")
  return { interfaceProp, setInterfaceProp }
}
