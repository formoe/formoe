// @flow
import { renderHook, act } from "@testing-library/react-hooks"
import useHookPlaceholder from "./index"

describe("useHookPlaceholder", () => {
  it("should work", () => {
    const { result: hookInterface } = renderHook(() => useHookPlaceholder())

    const { interfaceProp, setInterfaceProp } = hookInterface.current
    expect(interfaceProp).toBe("false")

    act(() => {
      setInterfaceProp("1337")
    })
    expect(interfaceProp).toBe("1337")
  })
})
