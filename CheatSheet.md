# Dev Cheat Sheet

## Common Pitfalls

[Pitfalls](Pitfalls.md)

## Usefull npm scripts

- `npm run bootstrap`: [Local Setup](#local-setup)
- `npm run init-package` variations to bootstrap a new package: [Setup new package](#setup-new-package)
- `npm run commit` and `npm run recommit`: [Commits](<#committing-(to)-your-work>)
- `npm run storybook`: Start storybook server

## Local setup

Clone repo and use `npm run bootstrap`.

## Setup new package

### General JS package

Use `npm run init-package:js --package=name` to bootstrap new react package.
Replace `name` with the name of your new package. Adhere to [npm package name rules](https://docs.npmjs.com/files/package.json#name) rules for the `name`.

Afterwards edit the following files in `packages/name`:

- `index.js` => main module entrypoint and probably all your code goes here
- `index.js.flow` => declare correct `flow` types so that package consumers are safe
- `index.spec.js` => write reasonable tests, cover as much as possible
- `package.json` => you might want to add description and stuff
- `README.md` => Tell others what the module is about

### React hook

Use `npm run init-package:hook --package=name --hook=hname` to bootstrap a new themed component. Replace `name` with the name of your new package and `hname` with that of the hook in _upper case_ and _without_ the `use-` prefix. So for example to create the use-toggle hook: `npm run init-package:hook --package=use-toogle --hook=Toggle`. Adhere to [npm package name rules](https://docs.npmjs.com/files/package.json#name) rules for the `name`.

Edit the above files.

### React component

Use `npm run init-package:component --package=name --component=cname` to bootstrap a new themed component. Replace `name` with the name of your new package and `cname` with that of the component. Adhere to [npm package name rules](https://docs.npmjs.com/files/package.json#name) rules for the `name`.

Files to edit in addition to the above:

- put stories in your `stories/component.stories` folder and show your component off in `storybook`

### Dependencies

`npm run add -- [package] --scope=[consumer]` where `[consumer]` is replaced with the module you want to install into. For example `npm run add -- @material-ui/core --scope=@formoe/use-app-messages` installs the `@material-ui/core` package into the `@formoe/use-app-messages` module.

Use `npm run add:dev -- [package] --scope=[consumer]` to install a dev dependency.

Not specifying `--scope=[consumer]` will install the dependency into all packages.

To remove a _package_ from a _consumer_ package run `npm run remove --consumer=[consumer] --package=[package]` where `[consumer` ist the _package folder name_ so for example: `npm run remove --consumer=use-app-messages --package=@material-ui/core` to remove the `@material-ui/core` dependency from the `use-app-messages` package here. Be aware, that this differs slightly from the `add` command, which uses the full package qulaifier for the consumer!
Alternatively you can `npm uninstall` the package in the `packages/` folder and run `npm run bootstrap` afterwards.

#### Local dependencies

Local dependencies are a special case. By default `lerna` just symlinks the folder of a dependency found in the same repo into the `node_modules` folder of the consuming package. This is not desireable in our case for various tooling reasons. To work around this, you need to manually add a project dependency to your `package.json` specifying the `"latest"` dist tag. Thus lerna will always pull the package from the registry and hoist it to the root node modules folder.
Both `flow` and `storybook` rely on this to work correctly.

## Committing (to) your work

Use `npm run commit`!

If you don't (because you use an UI or need to `--amend` or something), make sure your message adheres to the configured commit message format. Amending will give you the message you previously entered, so no worries there.
If your commit message is in the wrong format, a commit hook will not let you commit.
**If you avoid the hook, you'll break the next publish!!! Public blaming and severe punishment will follow.**

Also make sure there are enough `tests`, `flow` is used correctly and your code is lintable. If you don't a commit hook will not let you commit. If you avoid the hook, you'll break the build.

### Recommitting

Use `npm run recommit` to retry a commit after a hook failed. Thus you don't have to enter everything again.
